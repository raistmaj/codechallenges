class Solution{
public:
  int dfs(int i, int j, vector<vector<int>>& grid){
    if(i == grid.size() - 1 && j == grid[0].size() - 1){
      return grid[i][j];
    }
    if(i < grid.size() -1 && j < grid[0].size() -1){
      int r1 = grid[i][j] + dfs(i+1,j,grid);
      int r2 = grid[i][j] + dfs(i,j+1,grid);
      return std::min(r1,r2);
    }
    if(i < grid.size() - 1){
      return grid[i][j] + dfs(i+1,j,grid);
    }
    if(j < grid[0].size() - 1){
      return grid[i][j] + dfs(i,j+1,grid);
    }
  }
  int minPathSum(vector<vector<int>>& grid){
    return dfs(0,0,grid);
  }
};
