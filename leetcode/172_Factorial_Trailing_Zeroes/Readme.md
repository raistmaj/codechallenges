Given an integer n, return the number of trailing zeroes in n!.

Note: Your solution should be in logarithmic time complexity.

Looks like the number of trailing zeros is associated with the number of numbers could be divided by 5