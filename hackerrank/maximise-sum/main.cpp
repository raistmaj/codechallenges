#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits>
#include <cstdlib>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int T;
    cin >> T;
    while(T > 0){
      int N,M;
      cin >> N >> M; 
      std::vector<int> singleArray(N);
      for(int i = 0; i < N; ++i){
        int value;
        cin >> value;
        singleArray[i] = value;
      }
      int MaxPossibleValue = (M - 1)%M;
      int maxvalue = 0;

      std::vector<int> prefix(N);
      int current = 0;
      for(int i = 0; i < N; ++i){
        prefix[i] = (singleArray[i] % M + current) % M;
      }
    
      int ret = 0;
      for(int i = 0; i < N; ++i) {
        for(int j = i-1; j >= 0; --j){
          ret = std::max(ret,(prefix[i] - prefix[j] + M) % M);
        }
        ret = std::max(ret,prefix[i]);
      }
      std::cout << ret << '\n';
      --T;
    }
    return 0;
}
