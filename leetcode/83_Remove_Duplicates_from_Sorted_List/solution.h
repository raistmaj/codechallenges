/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        if(head == nullptr) { return nullptr; }
        ListNode* p = head;
        while(p != nullptr){
            if(p->next != nullptr && p->next->val == p->val){
                ListNode* temp = p->next;
                p->next = temp->next;
                delete temp;
            }else{
                p = p->next;
            }
        }
        return head;
    }
};