#ifndef PROGRAM_CREEK_ALGORITHMS_1_ROTATE_ARRAY_REVERSE_STRING_H_H
#define PROGRAM_CREEK_ALGORITHMS_1_ROTATE_ARRAY_REVERSE_STRING_H_H

#include <iostream>
#include <vector>
#include <algorithm>

namespace one {

  /*
    Rotate an array of n elements to the right by k steps.
    For example, with n = 7 and k = 3, the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].
    How many different ways do you know to solve this problem?
  */
  class rotate_array {
  public:
    template<typename T>
    static void reverse_local(T &begin, T &end) {
      while (begin < end) {
        std::swap(*begin, *end);
        begin++;
        end--;
      }
    }

    template<typename T>
    static std::vector<T> rotate_copy(const std::vector<T> &input, unsigned int k) {
      // The fastest way to rotate an array is to use reverse
      // reverse 0, k
      // reverse k, n
      // reverse 0, n
      std::vector<T> ret_val(input);
      if (k > 0) {
        std::reverse(ret_val.begin(), ret_val.begin() + k + 1);
        std::reverse(ret_val.begin() + k + 1, ret_val.end());
        std::reverse(ret_val.begin(), ret_val.end());
      }
      return ret_val;
    }

    static void test() {
      std::vector<int> input_vector{1, 2, 3, 4, 5, 6, 7};
      std::vector<int> expected_output{5, 6, 7, 1, 2, 3, 4};
      auto ouput = rotate_copy(input_vector, 3);
      if (expected_output != ouput) {
        std::cout << "Error, input != output\n";
        for (auto &i : ouput) {
          std::cout << i << " ";
        }
        std::cout << '\n';
        for (auto &i: expected_output) {
          std::cout << i << " ";
        }
        std::cout << '\n';
      }
    }
  };

  /**
    Given an input string, reverse the string word by word. A word is defined as a sequence of non-space characters.

    The input string does not contain leading or trailing spaces and the words are always separated by a single space.

    For example,
    Given s = "the sky is blue",
    return "blue is sky the".

    Could you do it in-place without allocating extra space?
   * */
  class rotate_words {
  public:
    static std::string reverse_words(const std::string &input) {
      std::string ret_val(input);
      std::string::size_type actual_word = 0;
      for (std::string::size_type i = 0; i < ret_val.size(); ++i) {
        if (std::isspace(ret_val[i])) {
          std::reverse(ret_val.begin() + actual_word, ret_val.begin() + i);
          actual_word = i + 1;
        }
      }

      std::reverse(ret_val.begin() + actual_word, ret_val.end());
      std::reverse(ret_val.begin(), ret_val.end());

      return ret_val;
    }

    static void test() {
      std::string input{"the sky is blue"};
      std::string expected_output{"blue is sky the"};

      std::string output{reverse_words(input)};
      std::cout << expected_output << (expected_output == output ? " == " : " != ") << output << std::endl;
    }
  };
}

#endif //PROGRAM_CREEK_ALGORITHMS_1_ROTATE_ARRAY_REVERSE_STRING_H_H
