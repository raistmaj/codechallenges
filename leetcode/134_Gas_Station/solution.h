class Solution{
public:

  int canCompleteCircuit(std::vector<int> gas, std::vector<int> cost){
    int sumRemaining = 0; // track current
    int total = 0;  // track total
    size_t start = 0; 

    for(size_t i = 0; i < gas.size(); ++i) {
      int remaining = gas[i] - cost[i];
      
      // is the remaining (i-1) steps enough?
      if(sumRemaining >= 0){
        sumRemaining += remaining;
      } else {
        // Otherwise restart the start to our current index
        sumRemaining = remaining;
        start = i;
      }
      total += remaining;
    }

    if(total >= 0){
      return start;
    }else{
      return -1;
    }
  }
};
