class Solution {
public:
    vector<int> getRow(int rowIndex) {
        int mRow = rowIndex + 1;
        std::vector<vector<int>> pascalT(mRow);
        for(int i = 0; i < mRow; ++i){
            pascalT[i] = std::vector<int>((i+1),0);
        }
        for(int i = 0; i < mRow; ++i){
            pascalT[i][0] = pascalT[i][pascalT[i].size()-1] = 1;
        }
        if(mRow > 2){
            for(int i = 2; i< mRow; ++i){
                for(int j = 1; j < pascalT[i].size() - 1; ++j){
                    pascalT[i][j] = pascalT[i-1][j-1] + pascalT[i-1][j];
                }
            }
        }
        return pascalT[mRow-1];
    }
};