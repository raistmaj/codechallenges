class Solution {
public:
	/**
	Instead of keeping track of largest element in the array, we track the maximum proﬁt so far.
	*/
    int maxProfit(vector<int>& prices) {
        int profit = 0;
        int minElement = std::numeric_limits<int>::max();
        for(int i = 0; i < prices.size(); ++i){
        	profit = std::max(profit,prices[i]-minElement);
        	minElement = std::min(minElement,prices[i]);
        }
        return profit;
    }
};