class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        if(obstacleGrid.empty() || obstacleGrid[0].empty()) return 0;

        int m = obstacleGrid.size();
        int n = obstacleGrid[0].size();
    
        // Case at beginning or at end we have obstacle
        if(obstacleGrid[0][0] == 1 || obstacleGrid[m-1][n-1] == 1){
          return 0;
        }
    
        std::vector<std::vector<int>> paths(m);
        for(int i = 0; i < m; ++i){
          paths[i].resize(n);
        }
        paths[0][0] = 1;
    
        // Left column
        for(int i = 1; i < m; ++i){
          if(obstacleGrid[i][0] == 1){
            paths[i][0] = 0;
          }else{
            paths[i][0] = paths[i-1][0];
          }
        }
        // top row
        for(int j = 1; j < n; ++j){
          if(obstacleGrid[0][j] == 1){
            paths[0][j] = 0;
          }else{
            paths[0][j] = paths[0][j-1];
          }
        }
        // Fill The rest
        for(int i = 1; i < m; ++i){
          for(int j = 1; j < n; ++j){
            if(obstacleGrid[i][j] == 1){
              paths[i][j] = 0;
            }else{
              paths[i][j] = paths[i-1][j] + paths[i][j-1];
            }
          }
        }
        return paths[m-1][n-1];
    }
};