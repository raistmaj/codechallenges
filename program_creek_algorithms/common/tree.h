//
// Created by jpalma on 8/02/17.
//

#ifndef CODECHALLENGES_TREE_H
#define CODECHALLENGES_TREE_H

namespace common {
	template<typename T>
	class tree_node {
	public:

		T value;
		tree_node<T> *left = nullptr;
		tree_node<T> *right = nullptr;
	};
}

#endif //CODECHALLENGES_TREE_H
