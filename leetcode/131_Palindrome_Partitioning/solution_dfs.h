class Solution {
public:
	void addPalindrome(string& s, int start, vector<string> part, std::vector<vector<string>>& result){
		if(start == s.length()){
			result.push_back(part);
			return;
		}
		for(int i = start; i < s.length(); ++i){
			string str = s.substr(start,i-start+1);
			if(isPalindrome(str)){
				part.push_back(str);
				addPalindrome(s,i+1,part,result);
				part.pop_back();
			}
		}
	}

	bool isPalindrome(const std::string& str){
		int left = 0;
		int right = str.length() - 1;
		while(left < right){
			if(str[left] != str[right]){
				return false;
			}
			left++;
			right--;
		}
		return true;
	}

    vector<vector<string>> partition(string s) {
        vector<vector<string>> result;
        if(s.length() == 0){
        	return result;
        }

        vector<string> part;
        addPalindrome(s,0,part,result);
        return result;
    }
};