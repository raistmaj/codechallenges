/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
#include <stack>
class BSTIterator {
public:
  std::stack<TreeNode *> lstack;

  BSTIterator(TreeNode *root) {
    while (root != nullptr) {
      lstack.push(root);
      root = root->left;
    }
  }
  /** @return whether we have a next smallest number */
  bool hasNext() { return !lstack.empty(); }
  /** @return the next smallest number */
  int next() {
    TreeNode *node = lstack.top();
    lstack.pop();
    int retVal = node->val;
    if (node->right != nullptr) {
      node = node->right;
      while (node != nullptr) {
        lstack.push(node);
        node = node->left;
      }
    }
    return retVal;
  }
};

/**
 * Your BSTIterator will be called like this:
 * BSTIterator i = BSTIterator(root);
 * while (i.hasNext()) cout << i.next();
 */
