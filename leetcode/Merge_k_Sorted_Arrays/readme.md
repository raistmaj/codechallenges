This is a classic interview question. Another similar problem is "merge k sorted lists".
This problem can be solved by using a heap. The time is O(nlog(n)).
Given m arrays, the minimum elements of all arrays can form a heap.
It takes O(log(m)) to insert an element to the heap and it takes O(1) to delete the minimum element.
