bool isPalindrome(string s) {
       if(s.empty()) return true;
       // Preprocess string to avoid weird things
       std::string toProcess;
       for(size_t i = 0; i < s.length(); ++i){
           if(isalnum(s[i])){
               toProcess += tolower(s[i]);
           }
       }
       int left = 0;
       int right = toProcess.length() - 1;
       while(left < right){
           if(toProcess[left] != toProcess[right]){
               return false;
           }
           left++;
           right--;
       }
       return true;
   }
