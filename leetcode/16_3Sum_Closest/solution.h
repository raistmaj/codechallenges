#include <cmath>

class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {
        size_t start = 0;
        size_t end = 0;
        int sum = 0;
        int newDiff = 0;
        if (nums.size() < 3) return 0;
        int closet = nums[0] + nums[1] + nums[2];
        int actualDiff = abs(closet-target);
        int arraySize = nums.size();
        std::sort(nums.begin(), nums.end());
        for(size_t i = 0; i < arraySize- 2; ++i) {
            start = i + 1;
            end = arraySize - 1;
            while(start < end){
                sum = nums[i] + nums[start] + nums[end];
                newDiff = (abs(sum-target));
                if(newDiff < actualDiff){
                    actualDiff = newDiff;
                    closet = sum;
                }
                if(sum < target){
                    start++;
                }else{
                    end--;
                }
            }
        }
        return closet;
    }
};