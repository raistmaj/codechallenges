class Solution {
public:
    string addBinary(string a, string b) {
        if(a.empty()) return b;
        if(b.empty()) return a;
        std::string result;
        
        int endA = a.length() - 1;
        int endB = b.length() - 1;
        
        int carry = 0;
        while(endA >= 0 && endB >= 0){
            if(a[endA] == '1' && b[endB] == '1') {
                if(carry){
                    result = '1' + result;
                }
                else{
                    result = '0' + result;
                    carry = 1;
                }
            }else if(a[endA] == '0' && b[endB] == '0'){
                if(carry){
                    result = '1' + result;
                    carry = 0;
                }else{
                    result = '0' + result;
                }
            }else{
                if(carry){
                    result = '0' + result;
                    carry = 1;
                }else{
                    result = '1' + result;
                }
            }
            endA--;
            endB--;
        }
        // A has pending data
        while(endA >= 0){
            if(a[endA] == '0'){
                if(carry){
                    carry = 0;
                    result = '1' + result;
                }else{
                    result = '0' + result;
                }
            }else if(a[endA] == '1'){
                if(carry){
                    result = '0' + result;
                }else {
                    result = '1' + result;
                    carry = 0;
                }
            }
            endA--;
        }
        // B has pending data
        while(endB >= 0){
            if(b[endB] == '0'){
                if(carry){
                    carry = 0;
                    result = '1' + result;
                }else{
                    result = '0' + result;
                }
            }else if(b[endB] == '1'){
                if(carry){
                    result = '0' + result;
                }else {
                    result = '1' + result;
                    carry = 0;
                }
            }
            endB--;
        }
        if(carry){
            result = '1' + result;
        }
        return result;
    }
};