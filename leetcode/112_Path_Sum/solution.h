/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool suma(TreeNode* node, int sum, int actualVal){
        int actVal = actualVal + node->val;
        if(node->left == nullptr && node->right == nullptr){
            return actVal == sum;
        }
        bool left = false;
        bool right = false;
        if(node->left != nullptr)
            left = suma(node->left,sum,actVal);
        if(node->right != nullptr)
            right = suma(node->right,sum,actVal);
        return left || right;
    }

    bool hasPathSum(TreeNode* root, int sum) {
        if(root == nullptr) return false;
        if(root->left == nullptr && root->right == nullptr){
            return root->val == sum;
        }

        bool left = false;
        bool right = false;

        int actualVal = root->val;
        if(root->left != nullptr)
            left = suma(root->left,sum,actualVal);
        if(root->right != nullptr)
            right = suma(root->right,sum,actualVal);
        
        return left || right;
    }
};