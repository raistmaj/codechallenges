class Solution {
public:
    
    void generateAlgo(int left, int right, string actual, std::vector<string>& results){
        if(left > right){ // More ) than (
            return;
        }
        if(left == 0 && right == 0){
            results.push_back(actual);
        }
        if(left > 0){
            generateAlgo(left-1,right,actual + "(", results);
        }
        if(right > 0){
            generateAlgo(left, right-1, actual + ")", results);
        }
    }

    vector<string> generateParenthesis(int n) {
        if(n == 0){ return std::vector<std::string>(); }
        std::vector<string> results;
        generateAlgo(n,n,"", results);
        return results;
    }
};