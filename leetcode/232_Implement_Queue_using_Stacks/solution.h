#include <stack>

class Queue {
public:
    std::stack<int> m_internal;
    // Push element x to the back of queue.
    void push(int x) {
        std::stack<int> tmp;
        while(!m_internal.empty()){
            tmp.push(m_internal.top());
            m_internal.pop();
        }
        tmp.push(x);
        while(!tmp.empty()){
            m_internal.push(tmp.top());
            tmp.pop();
        }
    }

    // Removes the element from in front of queue.
    void pop(void) {
        m_internal.pop();
    }

    // Get the front element.
    int peek(void) {
        int retVal = m_internal.top();

    }

    // Return whether the queue is empty.
    bool empty(void) {
        return m_internal.empty();
    }
};
