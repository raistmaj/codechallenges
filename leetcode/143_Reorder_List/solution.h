/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
	ListNode* reverseOrder(ListNode* head){
		if(head == nullptr || head->next == nullptr){
			return head;
		}
		ListNode* pre = head;
		ListNode* curr = head->next;
		while(curr != nullptr){
			ListNode* tmp = curr->next;
			curr->next = pre;
			pre = curr;
			curr = tmp;
		}
		// Close
		head->next = nullptr;
		return pre;
	}
    void reorderList(ListNode* head) {
        if(head != nullptr && head->next != nullptr){
        	ListNode* slow = head;
        	ListNode* fast = head;

        	while(fast != nullptr && fast->next != nullptr && fast->next->next != nullptr){
        		slow = slow->next;
        		fast = fast->next->next;
        	}

        	ListNode* second = slow->next;
        	slow->next = nullptr; // Closed first part of the list

        	// Reverse the second part
        	second = reverseOrder(second);

        	// Merge two list
        	ListNode* p1 = head;
        	ListNode* p2 = second;
        	while(p2 != nullptr){
        		ListNode* temp1 = p1->next;
        		ListNode* temp2 = p2->next;

        		p1->next = p2;
        		p2->next = temp1;

        		p1 = temp1;
        		p2 = temp2;
        	}
        }
    }
};