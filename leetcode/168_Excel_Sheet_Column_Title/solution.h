class Solution {
public:
    const char c[26] = {'A', 'B', 'C', 'D', 'E', 
                      'F', 'G', 'H', 'I', 'J',
                      'K', 'L', 'M', 'N', 'O',
                      'P', 'Q', 'R', 'S', 'T',
                      'U', 'V', 'W', 'X', 'Y','Z'};
    
    string convertToTitle(int n) {
        std::string retVal;
        if(n <= 26){
          retVal += c[n-1];
          return retVal;
        } 
        while(n>0){
            int dec = (n-1) % 26;
            n = (n-1) / 26;
            retVal = c[dec] + retVal;
        }
        return retVal;
    }
};