class Solution {
public:
    void reverseWords(string &s) {
        /*if(s.length() == 0) return;
        
        int word_begin = 0;
        for(int i = 0; i < s.length();){
            i++;
            if( i >= s.length()){
                std::reverse(s.begin() + word_begin, s.begin() + i - 1);
            } 
            else if(s[i] == ' '){
                std::reverse(s.begin() + word_begin, s.begin() + i - 1);
                word_begin = i + 1;
            }
        }
        std::reverse(s.begin(),s.end());*/
        std::stringstream sb(s);
        std::vector<string> output;
        std::string aux;
        int i = 0;
        while(sb >> aux){
            output.push_back(aux);
        }
        std::reverse(output.begin(),output.end());
        s.clear();
        for(int i = 0; i < output.size(); ++i){
            s += output[i];
            if(i + 1 < output.size())
                s+= " ";
        }
    }
};