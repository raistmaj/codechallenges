/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        if(head == nullptr) { return nullptr; }
        
        ListNode* lastNewHead = new ListNode(head->val);
        while(head->next != nullptr){
            head = head->next;
            ListNode* newNode = new ListNode(head->val);
            newNode->next = lastNewHead;
            lastNewHead = newNode;
            
        }
        return lastNewHead;
    }
};