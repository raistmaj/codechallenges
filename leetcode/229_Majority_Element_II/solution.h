/**
 The solution with has table in o(n) in space
probably faster but as main difference I wanted to try
this thime the other approach. Very interesting.
*/
class Solution {
public:
    vector<int> majorityElement(vector<int>& nums) {
        std::vector<int> result;
        std::vector<int>::iterator n1 = nums.end();
        std::vector<int>::iterator n2 = nums.end();
        
        int c1 = 0, c2 = 0;
        
        for(auto i = nums.begin(); i != nums.end(); ++i){
            if(n1 != nums.end() && *i == *n1){
                c1++;
            }else if(n2 != nums.end() && *i == *n2){
                c2++;
            }else if(c1 == 0){
                c1 = 1;
                n1 = i;
            }else if(c2 == 0){
                c2 = 1;
                n2 = i;
            }else {
                c1--;
                c2--;
            }
        }
        c1 = c2 = 0;
        for(auto i = nums.begin(); i != nums.end(); ++i){
            if(*i == *n1){
                c1++;
            }else if(*i == *n2){
                c2++;
            }
        }
        if(c1 > nums.size() / 3){
            result.push_back(*n1);
        }
        if(c2 > nums.size() / 3){
            result.push_back(*n2);
        }
        return result;
    }
};