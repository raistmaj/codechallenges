class Solution {
public:
    vector<int> findOrder(int numCourses, vector<pair<int, int>>& prerequisites) {
        vector<int> schedule;
        if(numCourses == 0)
            return schedule;
            
        if(numCourses == 1) {
            schedule.push_back(numCourses - 1);
            return schedule;
        }
        
        int dep[numCourses] = {0};
        for(int i = 0; i < prerequisites.size(); ++i)
            ++dep[prerequisites[i].first];
        
        queue<int> no_pre_courses;
        for(int start = 0; start < numCourses; ++start) {
            if(dep[start] == 0)
                no_pre_courses.push(start);
        }
        
        while(!no_pre_courses.empty()) {
            int course = no_pre_courses.front();
            no_pre_courses.pop();
            schedule.push_back(course);
            for(vector<pair<int, int> >::iterator it = prerequisites.begin(); it != prerequisites.end() && !prerequisites.empty();) {
                if((*it).second != course) {
                    ++it;
                    continue;
                }
                
                if(--dep[(*it).first] == 0)
                    no_pre_courses.push((*it).first);
                it = prerequisites.erase(it);
            }
        }
        
        return (prerequisites.empty() ? schedule : vector<int>());
    }
};