/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void minimumDepth(TreeNode* node, int actualDepth, int& md){
        if(actualDepth >= md) return; // Do not continue on this branch
        if(node->left == nullptr && node->right == nullptr){
            if(actualDepth < md){
                md = actualDepth;
            }
        }
        if(node->left != nullptr)
            minimumDepth(node->left,actualDepth+1,md);
        if(node->right != nullptr)
            minimumDepth(node->right,actualDepth+1,md);
        
    }

    int minDepth(TreeNode* root) {
        if(root == nullptr) return 0;
        int actualDepth = 1;
        if(root->left == nullptr && root->right == nullptr){
            return 1;
        }
        int minimumDepth_ = std::numeric_limits<int>::max();
        if(root->left != nullptr){
            minimumDepth(root->left,actualDepth+1,minimumDepth_);
        }
        if(root->right != nullptr){
            minimumDepth(root->right,actualDepth+1,minimumDepth_);
        }
        return minimumDepth_;
    }
};