/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void dfs(TreeNode* node, int sum, vector<vector<int>>& result, vector<int> actualResult){
		if(node->left == nullptr && node->right == nullptr && sum == 0){
			result.push_back(actualResult);
		}
		if(node->left != nullptr){
			actualResult.push_back(node->left->val);
			dfs(node->left,sum-node->left->val,result,actualResult);
			actualResult.pop_back();
		}
		if(node->right != nullptr){
			actualResult.push_back(node->right->val);
			dfs(node->right,sum-node->right->val,result,actualResult);
			actualResult.pop_back();
		}

	}
    vector<vector<int>> pathSum(TreeNode* root, int sum) {
    	vector<vector<int>> retval;
        if(root == nullptr){
        	return retval;
        }
        vector<int> l;
        l.push_back(root->val);
        dfs(root,sum-root->val,retval,l);
        return retval;
    }
};