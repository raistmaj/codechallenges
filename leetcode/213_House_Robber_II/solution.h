class Solution {
public:
    int rob(vector<int>& nums) {
        if(nums.empty()){
            return 0;
        }
        
        int n = nums.size();
        if(n == 1){
            return nums[0];
        }
        if(n == 2){
            return std::max(nums[1],nums[0]);
        }
        
        std::vector<int> dp(n+1);
        dp[0] = 0;
        dp[1] = nums[0];
        
        for(int i = 2; i < n; ++i){
            dp[i] = std::max(dp[i-1],dp[i-2]+nums[i-1]);
        }
        
        std::vector<int> dr(n+1);
        dr[0] = 0;
        dr[1] = nums[1];
        for( int i = 2; i < n; ++i){
            dr[i] = std::max(dr[i-1],dr[i-2]+nums[i]);
        }
        return std::max(dp[n-1],dr[n-1]);
    }
};