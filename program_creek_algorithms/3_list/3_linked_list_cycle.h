#ifndef PROGRAM_CREEK_ALGORITHMS_3_LINKED_LIST_CYCLE_H
#define PROGRAM_CREEK_ALGORITHMS_3_LINKED_LIST_CYCLE_H

#include "../common/list.h"

namespace three {
  /*Given a linked list, determine if it has a cycle in it.*/
  class list_cycle {
  public:
    template <typename T>
    static bool cycle(common::list_node<T> *a) {

      common::list_node<T> *slow = a;
      common::list_node<T> *fast = a;

      while(fast != nullptr && fast->next != nullptr) {
        slow = slow->next;
        fast = fast->next->next;
        if (slow == fast) {
          return true;
        }
      }
      return false;
    }
  };
}

#endif //PROGRAM_CREEK_ALGORITHMS_3_LINKED_LIST_CYCLE_H
