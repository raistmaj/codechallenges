class Solution {
public:
    int findMin(vector<int>& num, int left, int right){
        if(left == right){
            return num[left];
        }
        if(right == left + 1){
            return std::min(num[left],num[right]);
        }
        // 3 3 1 3 3 3 
        int mid = (right - left) / 2 + left;
        // Already sorted
        if(num[right] > num[left]){
            return num[left];
        // Right shift one
        }else if(num[right] == num[left]){
            return findMin(num, left + 1, right);
        // Go right
        }else if(num[mid] >= num[left]){
            return findMin(num, mid, right);
        // Go left
        }else{
            return findMin(num, left, mid);
        }
    }
    
    int findMin(vector<int>& nums) {
        return findMin(nums,0,nums.size()-1);
    }
};