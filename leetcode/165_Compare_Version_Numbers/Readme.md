Compare two version numbers version1 and version2. If version1 >version2 return 1,
if version1 <version2 return -1, otherwise return 0.
You may assume that the version strings are non-empty and contain only digits and
the . character. The . character does not represent a decimal point and is used to
separate number sequences.
Here is an example of version numbers ordering

0.1 < 1.1 < 1.2 < 13.37
