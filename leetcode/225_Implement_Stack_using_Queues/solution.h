#include<queue>

class Stack {
public:
   std::queue<int> m_queue;
  // Push element x onto stack.
  void push(int x) {
    // Need to invert insert and revert
    m_queue.push(x);
  }

  // Removes the element on top of the stack.
  void pop() {
    size_t queueSize = m_queue.size();
    std::queue<int> aux;
    while (queueSize > 1) {
      int t_ = m_queue.front();
      m_queue.pop();
      queueSize--;
      aux.push(t_);
    }
    m_queue = aux;
  }

  // Get the top element.
  int top() {
    return m_queue.back();
  }

  // Return whether the stack is empty.
  bool empty() {
    return m_queue.empty();
  }
};