Given a string S and a string T, count the number of distinct subsequences of T in S.

A subsequence of a string is a new string which is formed from the original string by deleting
some (can be none) of the characters without disturbing the relative positions of the remaining characters.
(ie, "ACE" is a subsequence of "ABCDE" while "AEC" is not).

Here is an example:
S = "rabbbit", T = "rabbit"

Return 3.

-----------


When you see string problem that is about subsequence or matching, dynamic programming method should come to
your mind naturally. The key is to ﬁnd the changing condition.

Let W(i, j) stand for the number of subsequences of S(0, i) in T(0, j).
If S.charAt(i) == T.charAt(j),
 W(i, j) = W(i-1, j-1) + W(i-1,j);
Otherwise, W(i, j) = W(i-1,j).
