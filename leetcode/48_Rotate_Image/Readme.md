You are given an n x n 2D matrix representing an image.
Rotate the image by 90 degrees (clockwise).
Follow up: Could you do this in-place?

By using the relation "matrix[i][j] = matrix[n-1-j][i]", we can loop through the matrix.


Analysis
The classic problem in Career Cup book 1.6.
The key idea is to rotate the matrix according to layers. For the nth layer(the out layer), rotate 90 degree is to move all the elements n times in a circle. In each layer, the rotation can be performed by first swap 4 corners, then swap 4 elements next to corner until the end of each line.

