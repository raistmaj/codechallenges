class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        std:deque<int> a(nums.begin(),nums.end());
        for(int i = 0; i < k; ++i){
            int aux = a.back();
            a.pop_back();
            a.push_front(aux);
        }
        nums = std::vector<int>(a.begin(),a.end());
    }
};
