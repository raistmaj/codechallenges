class Solution {
public:
    int numDistinct(string s, string t) {
        std::vector<std::vector<int>> table(s.size() +1, std::vector<int>(t.size()+1));

        // Init left column
        for(int i = 0; i < s.size(); ++i){
        	table[i][0] = 1;
        }
        for(int i = 1; i <= s.size(); ++i){
        	for(int j = 1; j <= t.size(); ++j){
        		if(s[i-1] == t[j-1]){
        			table[i][j] += table[i-1][j] + table[i-1][j-1];
        		}else{
        			table[i][j] += table[i-1][j];
        		}
        	}
        }
        return table[s.size()][t.size()];
    }
};