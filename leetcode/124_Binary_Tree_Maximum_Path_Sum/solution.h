/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	int calculatePathSum(TreeNode* root, std::vector<int>& maximum){
		if(root == nullptr){
			return 0;
		}
		int left = calculatePathSum(root->left, maximum);
		int right = calculatePathSum(root->right, maximum);

		int current = std::max(root->val, std::max(root->val + left, root->val + right));

		maximum[0] = std::max(maximum[0], std::max(current, left + root->val + right));

		return current;
	}
    int maxPathSum(TreeNode* root) {
        std::vector<int> maximum(1,std::numeric_limits<int>::min());
        calculatePathSum(root,maximum);
        return maximum[0];
    }
};