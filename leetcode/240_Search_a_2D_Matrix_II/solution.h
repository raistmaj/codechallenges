#include <algorithm>

class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        // Rows
        for(int i = 0; i < matrix.size(); ++i){
            if(matrix[i][0] > target) break;
            // Columns
            bool retVal = std::binary_search(matrix[i].begin(),matrix[i].end(),target);
            if(retVal){
                return true;
            }
        }
        return false;
    }
};
