class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        if(strs.empty()) return std::string();
        if(strs[0].empty()) return std::string();
        
        std::string retVal;
        int actualCharPosition = 0;
        bool end = false;
        do{
            if (actualCharPosition >= strs[0].size()) {
              break;
            }
            int actualChar = strs[0][actualCharPosition];
            for(int i = 1; i < strs.size(); ++i){
                if(actualCharPosition >= strs[i].size()){
                    return retVal;
                }
                if(actualChar != strs[i][actualCharPosition]){
                    return retVal;
                }
            }
            if(!end){
                retVal += actualChar;
                ++actualCharPosition;
            }
        }while(!end);
        return retVal;
    }
};