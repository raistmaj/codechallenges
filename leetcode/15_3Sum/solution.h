#include <algorithm>

class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        std::vector<std::vector<int>> result;
        if(nums.size() < 3) return result;
        std::sort(nums.begin(),nums.end());
        
        for(size_t i = 0; i < nums.size() - 2; ++i) {
            if(i == 0 || nums[i] > nums[i - 1]){
                int negate = -nums[i];
                
                size_t start = i + 1;
                size_t end = nums.size() - 1;
                while(start < end){
                    if(nums[start] + nums[end] == negate){
                        result.push_back(std::vector<int>({nums[i],nums[start],nums[end]}));
                        start++;
                        end--;
                        // Avoid duplicates
                        while(start < end && nums[end] == nums[end + 1])
                            end--;
                        while(start < end && nums[start] == nums[start - 1])
                            start++;
                    }else if(nums[start] + nums[end] < negate){
                        start++;
                    }else{
                        end--;
                    }
                }
            }
        }
        return result;
    }
};