Write a function to find the longest common prefix string amongst an array of strings.


----


The list of strings will be of different size so in case we find the end of one, we can stop the process and return the value.
As it is a prefix, in the moment we have a different character we can finish, on any other case append the actual character.
