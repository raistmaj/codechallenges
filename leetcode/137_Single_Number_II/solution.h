#include <unordered_map>

class Solution {
public:
  int singleNumber(vector<int> &nums) {
    int result = 0;
    std::unordered_map<int, int> setsNum;
    for (auto i : nums) {
      if (setsNum.find(i) == setsNum.end())
        setsNum[i] = 0;
      else
        setsNum[i] += 1;
    }
    for (auto it = setsNum.begin(); it != setsNum.end(); ++it) {
      if (it->second == 0) {
        return it->first;
      }
    }
  }
};
