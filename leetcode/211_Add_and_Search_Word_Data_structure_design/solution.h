
class TrieNode {
public:
  TrieNode() {}
  TrieNode(char ca) : c(ca) {}

  bool isLeaf = false;
  char c;
  map<char, TrieNode*> children;
};

class WordDictionary {
private:
  TrieNode* root;
public:
  WordDictionary() {
    root = new TrieNode();
  }

  // Adds a word into the data structure.
  void addWord(string word) {
    TrieNode* actualNode = root;
    for (int i = 0; i < word.length(); ++i) {
      char c = word[i];
      auto contains = actualNode->children.find(c);
      if (contains != actualNode->children.end()) {
        actualNode = contains->second;
      }
      else {
        TrieNode* t = new TrieNode(c);
        actualNode->children[c] = t;
        actualNode = t;
      }
      if (i == word.length() - 1) {
        actualNode->isLeaf = true;
      }
    }
  }

  // Returns if the word is in the data structure. A word could
  // contain the dot character '.' to represent any one letter.
  bool search(string word) {
    return dfs(root, word, 0);
  }

  bool dfs(TrieNode* node, string& word, int actualWordPosition) {
    if (actualWordPosition >= word.length()) {
      if (node->children.size() == 0) {
        return true;
      }
      else {
        return false;
      }
    }
    char c = word[actualWordPosition];
    if (c == '.') {
      for (auto it = node->children.begin(); it != node->children.end(); ++it) {
        if (actualWordPosition == word.length() - 1 && it->second->isLeaf) {
          return true;
        }
        if (dfs(it->second, word, actualWordPosition + 1)) {
          return true;
        }
      }
      return false;
    }
    else {
      auto contains = node->children.find(c);
      if (contains != node->children.end()) {
        if (actualWordPosition == word.length() - 1 && contains->second->isLeaf) {
          return true;
        }
        return dfs(contains->second, word, actualWordPosition + 1);
      }
      else {
        return false;
      }
    }
  }
};