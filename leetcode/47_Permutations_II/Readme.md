Given a collection of numbers that might contain duplicates, return all possible unique permutations.

For example,
[1,1,2] have the following unique permutations:
[1,1,2], [1,2,1], and [2,1,1].

For each number in the array, swap it with every element after it. To avoid duplicate, we need to check the existing sequence ﬁrst.


Analysis:
Facing this kind of problem, just consider this is a similar one to the previous(see here), but need some modifications. In this problem, what we need it to cut some of the subtrees.  e.g. 122
                     122
         /             |           \
     122              212         X  (here because 2=2, we don't need to swap again)
    /     \          /    \
 122      X         212 221 

So, if there exist same element after current swap, there there is no need to swap again.
Details see code.