class Solution {
public:
  string simplifyPath(string path) {
    std::stack<std::string> st;
    int i = 0;
    // Remove double //
    while (i < path.length() - 1) {
      if (path[i] == '/' && path[i + 1] == '/') {
        path.erase(i, 1);
      }
      else {
        ++i;
      }
    }
    // If there is no / at the endd include it
    if (path[path.length() - 1] != '/') { path += "/"; }

    string aux;
    int flag = 0;
    for (int i = 0; i < path.length(); ++i) {
      if (path[i] == '/') { flag++; }
      if (flag == 1) { aux += path[i]; }
      if (flag == 2) {
        if (aux == "/.." && !st.empty()) {
          st.pop();
        }
        if (aux != "/." && aux != "/..") {
          st.push(aux);
        }
        flag = 1;
        aux = "/";
      }
    }

    if (st.empty()) { return "/";  }
    aux.clear();
    while (!st.empty()) {
      aux = st.top() + aux;
      st.pop();
    }
    return aux;
  }
};