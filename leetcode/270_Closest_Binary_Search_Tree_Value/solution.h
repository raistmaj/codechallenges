class Solution {
public:
    int closestValue(TreeNode* root, double target) {
        int ret = root->val;
        closestValue(root, target, ret);
        return ret;
    }
    void closestValue(TreeNode* root, double target, int &ret){
        if(root){
//            if(target == (double)(root->val)) {ret = target; return;}
            if(abs(root->val - target)<abs(ret - target)) 
                ret = root->val;
            if(target<(double)root->val) 
                closestValue(root->left, target, ret);
            else
                closestValue(root->right, target, ret);
        }
    }
};