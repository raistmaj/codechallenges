#ifndef CAS_CACHE_H
#define CAS_CACHE_H

#include <stdint.h>
#include <string>
#include <map>
#include <set>
#include <list>
#include <boost/smart_ptr.hpp>
#include <boost/thread/mutex.hpp>

/**
 * \author Jose Gerardo Palma Duran jpalma@barracuda.com
 * At development we found a big issue, we were scanning files
 * with extremely with wait times(linux kernel sources), and if somehow
 * those files were previously scanned, we didn't have any knowledge of
 * that and we rescaned the elements.
 * 
 * In atd we have our "own" hit database for files already sent
 * to the cloud. That database last for a time quite long and is
 * stored on disk.
 * 
 * We need a local cache, configurable in size/entries and life
 * time of entries.
 * 
 * Cache will store the hits and results of our scans, for good
 * and good files so we don't need to scan multiple times files 
 * already scanned.
 * 
 */ 
namespace cuda {
	/**
	 * Enum with the different adaptions we can use for the cache
	 */
	enum adaption_engine {
		none,
		avira,
		clamav,
		lastline
	};
	/**
	 * A cache configuration object represent the object
	 * we need to use to configure our cache in a easy and
	 * fancy way
	 * 
	 */
	class cache_configuration{
	public:
		/**
		 * Basic constructor. It will set the next values on the internal
		 * parameters
		 * 
		 * Default values are 100 Megabytes, 10 min per entry, 100000 entries
		 */
		inline cache_configuration(
			uint64_t maxSize = 10485760, 
			int64_t maxLifetime = 6000000, 
			int64_t maxEntries = 100000 )
		  : max_size_(maxSize),
		    max_lifetime_(maxLifetime),
		    max_entries_(maxEntries){
		}
		/**
		 * Copy constructor of the object
		 */
		inline cache_configuration(const cache_configuration& val)
		  : max_size_(val.max_size_),
		    max_lifetime_(val.max_lifetime_),
		    max_entries_(val.max_entries_){
		}
		/**
		 * Destructor of the cache configuration object
		 */
		inline ~cache_configuration(){
		}
		/**
		 * Assign operator of the cache configuration element
		 */
		inline cache_configuration& operator=(const cache_configuration& val){
			if(this != &val){
				max_size_ = val.max_size_;
				max_lifetime_ = val.max_lifetime_;
				max_entries_ = val.max_entries_;
			}
			return *this;
		}
		/**
		 * Gets the maximum size of the cache
		 */
		inline uint64_t max_size() const { return max_size_; }
		/**
		 * Sets the maximum size of the cache
		 */
		inline void set_size(uint64_t val) { max_size_ = val; }
		/**
		 * Gets the maximum lifetime of entries in the cache
		 */
		inline int64_t max_lifetime() const { return max_lifetime_; }
		/**
		 * Sets the maximum lifetime of entries in the cache
		 */
		inline void set_lifetime(int64_t val) { max_lifetime_ = val; }
		/**
		 * Gets the maximum number of entries allowed to be stored in the cache
		 */
		inline int64_t max_entries() const { return max_entries_; }
		/**
		 * Sets the maximum number of entries allowd to be stored in the cache
		 */
		inline void set_entries(int64_t val) { max_entries_ = val; } 
	protected:
		/**
		 * Max size of the cache in bytes. Max value is 2^64
		 * Quite big to be honest, can't image an appliance with that
		 * amount of memory
		 */
		uint64_t max_size_;
		/**
		 * Number of seconds we want to keep this entry in memory
		 * if value < 0 The entries will stay forever.
		 * 
		 * In that case, if we need to add a new entry on the cache
		 * we will remove the longest non-used entry.
		 */
		int64_t max_lifetime_;
		/**
		 * Number of entries we want to keep in memory
		 * if value < 0 The entries will stay forever
		 * 
		 * In case we need to add more entries and no memory is available
		 * we will remove the longest non-use entry
		 */
		int64_t max_entries_;
	};
	/**
	 * A cache entry on the for the cas service MUST contain
	 * all the required information to process a response in
	 * avira/clam/lastline
	 */
	class CacheEntry{
	public:
		/**
		 * Static method to compare two valid pointers of cache entry based on their time
		 */
		static bool less_ptr(boost::shared_ptr<CacheEntry> lhs, boost::shared_ptr<CacheEntry> rhs){
			if(lhs!=0 && rhs!=0){
				return lhs->m_lastModification < rhs->m_lastModification;
			}
			return false;
		}
	public:
		/**
		 * Basic Entry on the cache. Constructor
		 */
		inline CacheEntry() 
		  : m_hash(),
		    m_engine(none),
		    m_isVirus(false),
		    m_block(false),
		    m_lastModification(0),
		    m_xATD(false),
		    m_failOpen(false){}
		/**
		 * Destructor of the entry
		 */
		virtual ~CacheEntry(){}
		/**
		 * Gets the hash of the entry
		 */
		inline const std::string& getHash() const { return m_hash; }
		/**
		 * Sets the value of the hash of one entry
		 */
		inline void setHash(const std::string& val) { m_hash = val; }
		/**
		 * Mutable method of the hash of the entry
		 */
		inline std::string& mutableHash() { return m_hash; }
		/**
		 * Gets the engine used for this scan
		 */
		inline adaption_engine getEngine() const { return m_engine; }
		/**
		 * Sets the value of the engine used to scan this entry
		 */
		inline void setEngine(adaption_engine val) { m_engine = val; }
		/**
		 * Mutable method of the has of one entry
		 */
		inline adaption_engine& mutableEngine() { return m_engine; }
		/**
		 * Gets if the actual entry correspond to a virus or not
		 */
		inline bool getIsVirus() const { return m_isVirus; }
		/**
		 * Sets the value to detect if the entry is or not a virus
		 */
		inline void setIsVirus(bool value) { m_isVirus = value; }
		/**
		 * Mutable method of the is virus of one entry
		 */
		inline bool& mutableIsVirus() { return m_isVirus; }
		/**
		 * Gets the block flag
		 */
		inline bool getBlock() const { return m_block; }
		/**
		 * Sets the block flag
		 */
		inline void setBlock(bool val) { m_block = val; }
		/**
		 * Gets the reason
		 */
		inline const std::string& getReason() const { return m_xReason; }
		/**
		 * Sets the reason
		 */
		inline void setReason(const std::string& val) { m_xReason = val; }
		/**
		 * Gets the last modified time of the entry
		 */
		inline uint64_t getLastModification() const { return m_lastModification; }
		/**
		 * Sets the last modification time
		 */
		inline void setLastModification(uint64_t val) { m_lastModification = val; }
		/**
		 * Mutable method of the last modification entry
		 */
		inline uint64_t& mutableLastModification() { return m_lastModification; }
		/**
		 * Gets the flag to detect if it was a fail open hit
		 */
		inline bool getFailOpen() const { return m_failOpen; }
		/**
		 * Sets the flag to detect if it was a fail open
		 */
		inline void setFailOpen(bool val) { m_failOpen = val; }
		/**
		 * Mutable method to get the fail open flag
		 */
		inline bool& mutableFailOpen() { return m_failOpen; }
		/**
		 * Gets the flag to detect if the hit was produced by atd
		 */
		inline bool getATD() const { return m_xATD; }
		/**
		 * Sets the flag to detect if the hit was produced by atd
		 */
		inline void setATD(bool val) { m_xATD = val; }
		/**
		 * Mutable method to get the atd flag
		 */
		inline bool& mutableATD() { return m_xATD; }
		/**
		 * Gets the x violations found text
		 */
		inline const std::string& getViolationsFound() const { return m_xViolationsFound; }
		/**
		 * Sets the x violaitons text
		 */
		inline void setViolationsFound(const std::string& val) { m_xViolationsFound = val; }
		/**
		 * Mutable method for the xviolation text
		 */
		inline std::string& mutableViolationsFound() { return m_xViolationsFound; }
		/**
		 * Gets Blocked reason to add in the template
		 */
		inline const std::string& getBlockedReason() const { return m_wpBlockedReason; }
		/**
		 * Sets the blocked reason to add in the template
		 */
		inline void setBlockedReason(const std::string& val) { m_wpBlockedReason = val; }
		/**
		 * Mutable element block reason to add in the template
		 */
		inline std::string& mutableBlockedReason() { return m_wpBlockedReason; }
		/**
		 * Gets the alertname
		 */
		inline const std::string& getAlertName() const { return m_wpAlertName; }
		/**
		 * Sets the alertname
		 */
		inline void setAlertName(const std::string& val) { m_wpAlertName = val; }
		/**
		 * Mutable method to get the alert name
		 */
		inline std::string& mutableAlertName() { return m_wpAlertName; }
		/**
		 * Gets the alert description
		 */
		inline const std::string& getAlertDescription() const { return m_wpAlertDescription; }
		/**
		 * Sets the alert description
		 */
		inline void setAlertDescription(const std::string& val) { m_wpAlertDescription = val; }
		/**
		 * Mutbale method to get the alert description
		 */
		inline std::string& mutableAlertDescription() { return m_wpAlertDescription; }
		/**
		 * Gets the alert type
		 */
		inline const std::string& getAlertType() const { return m_wpAlertType; }
		/**
		 * Sets the alert type
		 */
		inline void setAlertType(const std::string& val) { m_wpAlertType = val; }
		/**
		 * Mutable element to work with the alert type
		 */
		inline std::string& mutableAlertType() { return m_wpAlertType; }
		/**
		 * Gets the risk of the atd thread
		 */
		inline const std::string& getRisk() const { return m_risk; }
		/**
		 * Sets the risk of the atd thread
		 */
		inline void setRisk(const std::string& val) { m_risk = val; }
		/**
		 * Gets the action of the atd block
		 */
		inline const std::string& getXAction() const { return m_xAction; }
		/**
		 * Sets the action of the atd block
		 */
		inline void setXAction(const std::string& val) { m_xAction = val; }
		/**
		 * Gets the size of the entry
		 * 
		 * The size is an approximation as we don't really now the whole string
		 * internal size, it should be it's size + the string content + some virtual
		 * stuff.
		 * 
		 * We use sizeof(*this) because the compiler will pad the class
		 * to optimize the field access (unless I set padding to 1 ofc).
		 * 
		 * This is not done merely because this cache MUST be fast.
		 * 
		 * Additionally the cache is managed with shared pointers so that will
		 * add more bytes to party that are not managed here
		 */
		inline uint64_t getSize() const {
			return sizeof(*this) + 
			  m_hash.size() +
			  m_xReason.size() + 
			  m_xViolationsFound.size() + 
			  m_wpBlockedReason.size() +
			  m_wpAlertDescription.size() +
			  m_wpAlertName.size() +
			  m_wpAlertType.size() +
			  m_xAction.size() + 
			  m_risk.size();
		}
		// Internal attribute DO NOT USE ELSEWHERE
		std::list<boost::shared_ptr<CacheEntry> >::iterator cache_position;
	protected: 
		/**
		 * SHA1 or similar value used to store the entry
		 */
		std::string m_hash;
		/**
		 * Engine used in the process to scan the file
		 */
		adaption_engine m_engine;
		/**
		 * Flag to mark if the entry is or isn't a virus/malware
		 */
		bool m_isVirus;
		/**
		 * Flag to mark if the file was blocked (this is only true if the flag m_isVirus is true)
		 */
		bool m_block;
		/**
		 * Last time modified
		 */
		uint64_t m_lastModification;
		/**
		 * Text to include in the X-Reason value of the ICAP response
		 */
		std::string m_xReason;
		// X-FILENAME is retrieved from the current request
		/**
		 * Was atd hit
		 */
		bool m_xATD;
		/**
		 * Fail open file
		 */
		bool m_failOpen;
		/**
		 * Text to include in case of X-Violations-Found
		 */
		std::string m_xViolationsFound;
		// HTML content to modify on the template library
		/**
		 * Blocked reason
		 */
		std::string m_wpBlockedReason;
		/**
		 * Alert name
		 */
		std::string m_wpAlertName;
		/**
		 * Alert type
		 */
		std::string m_wpAlertType;
		/**
		 * Alert Description
		 */
		std::string m_wpAlertDescription;
		/**
		 * X actio to be used by the atd block
		 */
		std::string m_xAction;
		/**
		 * Risk used in the atd system to communicate the value to the thread scan
		 */
		std::string m_risk;
	};
	//typedef std::pair<std::string,uint64_t> timeSetEntryType;
	typedef boost::shared_ptr<CacheEntry> timeSetEntryType;
	/**
	 * Trait used to compare the different cache entries
	 */
	class CompareCacheEntry{
	public:
		bool operator() (boost::shared_ptr<CacheEntry> lhs, boost::shared_ptr<CacheEntry> rhs) {
			if(lhs!=0 && rhs!=0){
				return lhs->getLastModification() < rhs->getLastModification();
			}
			return false;
		} 
// 		bool operator() (timeSetEntryType &lhs, timeSetEntryType &rhs) {
// 			if(lhs!=0 && rhs!=0){
// 				return lhs.second < rhs.second;
// 			}
// 			return false;
// 		} 
	};
	
	/**
	 * A cache instance represent a cache pool of entries with
	 * some fixed values we should need to specify. The parameters
	 * have fixed values by default you can configure at runtime.
	 */
	class Cache{
	public:
		/**
		 * The cache is coded using singleton pattern
		 */
		static Cache& getSingleton();
		/**
		 * Clean the resources used by the cache.
		 * 
		 * Elements are internally owned by the cache so unless you make
		 * a full copy of the element, take care with its life time
		 * as ALWAYS always < cache
		 * 
		 * As the data is hold accessed in a "unique" state, no data invalidation
		 * can happen, race condition, as the get will update the time
		 * of the key in that secure area and next calls will not clean it.
		 * 
		 * It is recommended by the user to control the life time of the objects
		 * by its own. That means, cache will have copies of the objects 
		 * the user must manage them.
		 */
		virtual ~Cache();
		/**
		 * Clear the internal elements of the cache and the entries
		 */
		void clear();
		/**
		 * Adds one element into the cache
		 * 
		 * Returns 1 in case the entry was added, 2 in case it exists
		 * and 0 in case couldn't be added
		 */
		int add(const CacheEntry& entry);
		/**
		 * Gets one entry if exits
		 * 
		 * Internally updates it last accessed time
		 */
		bool get(const std::string& key, CacheEntry& value);
		/**
		 * Updates de cache based on the configuration
		 */
		bool update();
		/**
		 * Updates one entry, in case it doesn't exist it will create it
		 */
		bool updateCreate(const CacheEntry& entry);
		/**
		 * Updates the configuration parameters of the cache
		 */
		void setConfiguration(const cache_configuration& config);
		/**
		 * Returns the number of hits
		 * 
		 * This method is NOT BLOCKING, so it may(and probably will) return a wrong value
		 * on multithreaded enviroment.
		 * 
		 * As is only an statistic value, we don't need 100% accuracy on it, and
		 * performance was chosen over that parameter
		 */
		inline uint32_t getNumberOfHits() const { return m_cacheHits; }
	protected:
		/**
		 * Basic cache constructor
		 * 
		 * It will build the basic cache
		 */
		Cache(const cache_configuration& config = cache_configuration());
		/**
		 * Map with the actual values we are managing internally
		 */
		std::map<std::string,boost::shared_ptr<CacheEntry> > m_entries;
// 		/**
// 		 * Set with the values. This is used to compare with the life time
// 		 * As sets are ordered red-black tree, we can iterate over
// 		 * them to detect entries in the right order
// 		 */
// 		std::set<timeSetEntryType ,cuda::CompareCacheEntry> m_lifeTime;
		std::list<boost::shared_ptr<CacheEntry> > m_lifeTime;
		/**
		 * Boost mutex used to protect the data for concurrent access
		 */
		boost::mutex m_resourceMutex;
		/**
		 * Local copy of the parameters
		 */
		cache_configuration m_configurationParameters;
		/**
		 * Size of the cache entries in bytes
		 */
		uint64_t m_cacheSize;
		/**
		 * Number of hits in the cache
		 */
		uint32_t m_cacheHits;
	};
	/**
	 * Helper class to store information in the request
	 * this is only used in that class, but to keep consistency
	 * in the development has been added in this file
	 */
	struct cache_data{
		/**
		 * Default constructor, will set atd to false
		 */
		cache_data() 
		  : atd(false), 
		    block(false),
		    hitengine(false),
		    failopen(false){}
		bool atd;
		bool block;
		bool hitengine;
		bool failopen;
		std::string reason;
		std::string violations;
		std::string blockR;
		std::string alertName;
		std::string alertType;
		std::string alertDescription;
		std::string actionAtd;
		std::string risk;
	};
}
#endif