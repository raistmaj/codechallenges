#include<unordered_map>

class Solution {
public:
    std::unordered_map<char,std::string> keypad = {{'0',""},
        {'2',"abc"},
        {'3',"def"},
        {'4',"ghi"},
        {'5',"jkl"},
        {'6',"mno"},
        {'7',"pqrs"},
        {'8',"tuv"},
        {'9',"wxyz"}};
    
    
    void getString(string digits, string& temp, std::vector<std::string>& result){
        if(digits.length() == 0){
            result.push_back(temp);
            return;
        }
        
        std::string letters = keypad[digits[0]];
        for(int i=0; i<letters.length(); i++){
            temp += letters[i];
            getString(digits.substr(1), temp, result);
            temp.pop_back();
        }
    }
    
    vector<string> letterCombinations(string digits) {
        std::vector<std::string> retval;
        if(digits.empty()) return retval;
        std::string temp;
        
        getString(digits, temp, retval);
    }
    
    
};