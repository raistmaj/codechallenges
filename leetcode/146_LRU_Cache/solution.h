class DoubleLinkedListNode {
public:
  DoubleLinkedListNode(int k, int value)
    : val(value), key(k) , pre(nullptr), next(nullptr) { }
  int val;
  int key;
  DoubleLinkedListNode* pre;
  DoubleLinkedListNode* next;
  
};

class LRUCache {
protected:
  std::unordered_map<int, DoubleLinkedListNode*> m_cache;
  DoubleLinkedListNode* head;
  DoubleLinkedListNode* end;
  int m_capacity;
  int m_len;

  void RemoveNode(DoubleLinkedListNode* node) {
    DoubleLinkedListNode* cur = node;
    DoubleLinkedListNode* pre = cur->pre;
    DoubleLinkedListNode* post = cur->next;

    if (pre != nullptr) {
      pre->next = post;
    }
    else {
      head = post;
    }
    if (post != nullptr) {
      post->pre = pre;
    }
    else {
      end = pre;
    }
  }
  void SetHead(DoubleLinkedListNode* node) {
    node->next = head;
    node->pre = nullptr;
    if (head != nullptr) {
      head->pre = node;
    }
    head = node;
    if (end == nullptr) {
      end = node;
    }
  }
public:
  LRUCache(int capacity)
    : m_capacity(capacity),
      m_len(0),
      head(nullptr),
      end(nullptr){
  }

  int get(int key) {
    int valToReturn = -1;
    auto pos = m_cache.find(key);
    if (pos != m_cache.end()) {
      // Reposition the node to the head
      DoubleLinkedListNode* latest = pos->second;
      RemoveNode(latest);
      SetHead(latest);
      valToReturn = latest->val;
    }
    return valToReturn;
  }

  void set(int key, int value) {
    auto pos = m_cache.find(key);
    if (pos != m_cache.end()) {
      DoubleLinkedListNode* oldNode = pos->second;
      oldNode->val = value;
      RemoveNode(oldNode);
      SetHead(oldNode);
    }
    else {
      DoubleLinkedListNode* newNode = new DoubleLinkedListNode(key, value);
      if (m_len < m_capacity) {
        SetHead(newNode);
        m_cache[key] = newNode;
        m_len++;
      }
      else {
        m_cache.erase(end->key);
        DoubleLinkedListNode* aux = end;
        end = end->pre;
        if (end != nullptr) {
          end->next = nullptr;
        }
        delete aux;
        SetHead(newNode);
        m_cache[key] = newNode;
      }
    }
  }
};