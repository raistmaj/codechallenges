#include <map>

class TwoSum {
public:
  void add(int num) {
    auto it = m_element.find(num);
    if (it != m_element.end()) {
      m_element[num] = m_element[num] + 1;
    }
    else {
      m_element.insert(std::make_pair(num, 1));
    }
  }

  bool find(int val) {
    auto it = m_element.begin();
    while (it != m_element.end()) {
      int target = val - it->first;
      auto containsIt = m_element.find(target);
      if (containsIt != m_element.end()) {
        if (it->first == target && containsIt->second < 2) {
          ++it;
          continue;
        }
        return true;
      }
      ++it;
    }
    return false;
  }
protected:
  /**
  * Elements stored on the Two sum object
  * we will check against them to check
  * for a possible match
  */
  std::map<int, int> m_element;
}