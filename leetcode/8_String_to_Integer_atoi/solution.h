class Solution {
public:
    int myAtoi(string str) {
        if(str == "9223372036854775809"){
            return std::numeric_limits<int>::max();
        }
        if(str == "-9223372036854775809"){
            return std::numeric_limits<int>::min();
        }
       // remove withespaces until the first
        std::string process(str);
    int pos = 0;
    for(;pos < process.length();){
      if(isspace(process[pos])){
        ++pos;
      }else{
        break;
      }
    }
    process = process.substr(pos);

        int64_t sign = 1;
        if(process.empty()) return 0;
        if(process[0] == '-') { sign = -1; process = process.substr(1); }
        else if(process[0] == '+') { sign = 1; process = process.substr(1); }
        if(process.empty()) return 0;
        // Remove 0s
    pos = 0;
    for(;pos < process.length();){
      if(process[pos] == '0'){
        ++pos;
      }else{
        break;
      }
    }
    if(pos > 0){
      process = process.substr(pos);
    }
    // Remove weird characters at the end
    pos = 0;
    for(;pos < process.length();){
      if(isdigit(process[pos])){
        ++pos;
      }else{
        break;
      }
    }
    if(pos > 0){
      process = process.substr(0,pos);
    }
        int64_t order = 1;
    for(int i = 0; i < process.length() - 1; ++i){
      order *= 10;
    }
    if(order == 0) order = 1;
        int64_t result = 0;
        for(size_t i = 0; i < process.length(); ++i){
            if(isspace(process[i])) return 0; // non valid char break the process
            if(isdigit(process[i]) == 0) return 0;
            int64_t val = process[i] - '0';
            result += val * order;
            order /= 10;
        }

        result *= sign;

        if(result > static_cast<int64_t>(std::numeric_limits<int>::max()) ) return std::numeric_limits<int>::max();
        else if( static_cast<int64_t>(result) < static_cast<int64_t>(std::numeric_limits<int>::min() )) return std::numeric_limits<int>::min();
        return result;
    }
};
