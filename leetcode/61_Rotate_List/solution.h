/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* rotateRight(ListNode* head, int k) {        
        if (head==NULL || k==0) {return head;}
         
        ListNode* p = head;
        int len=0;
        //get total length 
        while (p!=NULL){
            len++;
            p=p->next;
        }
        //get rotate position
        int r;
        if (k%len==0){
            return head;
        }else{
            r = len-(k%len)-1;    
        }
        p=head;
        while (r>0){
            p=p->next;
            r--;
        }
         
         
        //cut current list, link the back to the front
        ListNode *q =p->next;
        if (q==NULL){return head;}
        while (q->next!=NULL){
            q=q->next;
        }
        q->next = head;
        q=p->next;
        p->next=NULL;
         
        return q;
    }
};