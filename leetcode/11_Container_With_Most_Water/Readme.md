Given n non-negative integers a1, a2, ..., an, where each represents a point at coordinate (i, ai).
n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0).
Find two lines, which together with x-axis forms a container, such that the container contains the most water.

Note: You may not slant the container.

----


Need to iterate over left and right (0,end), and getting the maximum,
between the actual maximum and the distance X (right-left) * the minimum of the heights of those heights,
num[right], nums[left]