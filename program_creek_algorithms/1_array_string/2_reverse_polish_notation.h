#ifndef PROGRAM_CREEK_ALGORITHMS_2_REVERSE_POLISH_NOTATION_H
#define PROGRAM_CREEK_ALGORITHMS_2_REVERSE_POLISH_NOTATION_H

#include <stack>
#include <vector>
#include <iostream>

namespace one {
  /*
   * Evaluate the value of an arithmetic expression in Reverse Polish Notation.
   * Valid operators are +, -, *, /. Each operand may be an integer or another expression. For example:

  ["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9
  ["4", "13", "5", "/", "+"] -> (4 + (13 / 5)) -> 6
   *
   * */
  class polish_notation {
  public:
    static int evaluate(const std::vector<std::string> &input) {
      std::stack<int> polish;

      for (auto &i: input) {
        if (i == "+") {
          int first = polish.top();
          polish.pop();
          int second = polish.top();
          polish.pop();
          polish.push(first + second);
        } else if (i == "-") {
          int first = polish.top();
          polish.pop();
          int second = polish.top();
          polish.pop();
          polish.push(second - first);
        } else if (i == "*") {
          int first = polish.top();
          polish.pop();
          int second = polish.top();
          polish.pop();
          polish.push(first * second);
        } else if (i == "/") {
          int first = polish.top();
          polish.pop();
          int second = polish.top();
          polish.pop();
          polish.push(second / first);
        } else {
          polish.push(std::stoi(i));
        }
      }
      return polish.top();
    }

    static void test() {
      std::vector<std::string> one{"2", "1", "+", "3", "*"};
      std::vector<std::string> two{"4", "13", "5", "/", "+"};

      std::cout << "Evaluate one " << evaluate(one) << " - 9\n";
      std::cout << "Evaluate two " << evaluate(two) << " - 6\n";
    }
  };
}

#endif //PROGRAM_CREEK_ALGORITHMS_2_REVERSE_POLISH_NOTATION_H
