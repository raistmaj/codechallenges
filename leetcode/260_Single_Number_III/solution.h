class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        std::set<int> solution;
        for(auto& number: nums){
            auto posFind = solution.find(number);
            if( posFind == solution.end()){
                solution.insert(number);
            }else{ // duplicate
                solution.erase(posFind);
            }
        }
        return std::vector<int>(solution.begin(),solution.end());
    }
};
