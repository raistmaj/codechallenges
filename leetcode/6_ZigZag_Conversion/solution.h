class Solution {
public:
  string convert(string s, int numRows) {
    if (s.empty())
      return std::string();
    if (numRows == 1)
      return s;
    std::vector<std::vector<char>> matrix(numRows);
    for (auto &i : matrix) {
      i.resize(s.size(), ' ');
    }

    const int down = 1;
    const int up = 2;
    int currentDirection = down;
    int currentRow = 0;
    int currentColum = 0;
    for (int i = 0; i < s.length(); ++i) {
      char caracterToInsert = s[i];
      if (currentDirection == down) {
        matrix[currentRow][currentColum] = caracterToInsert;
        if (currentRow >= numRows - 1) {
          currentDirection = up;
          currentColum++;
          currentRow--;
          if (currentRow < 0)
            currentRow = 0;
        } else {
          currentRow++;
        }
      } else {
        matrix[currentRow][currentColum] = caracterToInsert;
        if (currentRow == 0) {
          currentRow++;
          currentDirection = down;
        } else {
          currentRow--;
          currentColum++;
        }
      }
    }
    std::string result;
    for (int i = 0; i < matrix.size(); ++i) {
      for (int j = 0; j < matrix[i].size(); ++j) {
        if (matrix[i][j] != ' ') {
          result += matrix[i][j];
        }
      }
    }
    return result;
  }
};
