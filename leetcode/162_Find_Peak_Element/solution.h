class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        if(nums[0] > nums[1]){
            return 0;
        }
        if(nums[nums.size()-2] < nums[nums.size()-1]){
            return nums.size() -1;
        }
        int a = 0;
        int b = 1;
        int c = 2;
        while(c < nums.size()){
            if(nums[a] < nums[b] && nums[b] > nums[c]){
                return b;
            }
            a++;
            b++;
            c++;
        }
    }
};