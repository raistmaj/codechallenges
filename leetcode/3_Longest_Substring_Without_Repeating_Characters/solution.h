class Solution {
public:
  int lengthOfLongestSubstring(string s) {
    std::deque<char> currC;
    int longestSeq = 0;
    for (int i = 0; i < s.length(); ++i) {
      char ac = s[i];
      auto position = std::find(currC.begin(), currC.end(), ac);
      if (position == currC.end()) {
        currC.push_back(ac);
        int actSeq = currC.size();
        if (actSeq > longestSeq) {
          longestSeq = actSeq;
        }
      } else {
        if (position == currC.begin()) {
          currC.pop_front();
          currC.push_back(ac);
          int actSeq = currC.size();
          if (actSeq > longestSeq) {
            longestSeq = actSeq;
          }
        } else {
          currC.erase(currC.begin(), position + 1);
          currC.push_back(ac);
          int actSeq = currC.size();
          if (actSeq > longestSeq) {
            longestSeq = actSeq;
          }
        }
      }
    }
    int actSeq = currC.size();
    if (actSeq > longestSeq) {
      longestSeq = actSeq;
    }
    return longestSeq;
  }
};
