/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    
    int currentLevel(TreeNode* root, int level){
        if(root == nullptr) return level;
        return std::max(currentLevel(root->left,level + 1),currentLevel(root->right, level + 1));
    }

    int maxDepth(TreeNode* root) {
        if(root == nullptr) return 0;
        return std::max(currentLevel(root->left,1),currentLevel(root->right,1));
    }
};