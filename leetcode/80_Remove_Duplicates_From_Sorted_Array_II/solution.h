class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if (nums.size() <= 2)
			return nums.size();
 
		int prev = 1; // point to previous
		int curr = 2; // point to current
 
		while (curr < nums.size()) {
			if (nums[curr] == nums[prev] && nums[curr] == nums[prev - 1]) {
				curr++;
			} else {
				prev++;
				nums[prev] = nums[curr];
				curr++;
			}
		}
		return prev + 1;
    }
};