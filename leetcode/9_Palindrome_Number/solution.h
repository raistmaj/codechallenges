class Solution {
public:
  bool isPalindrome(int x) {
    if (x < 0)
      return false;
    int n = x;
    int rev = 0;
    int dig;
    while (x > 0) {
      dig = (x % 10);
      rev = rev * 10 + dig;
      x *= 0.1;
    }
    return (n == rev);
  }
};