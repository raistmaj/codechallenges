class MinStack {
    std::stack<int> i;
    std::map<int,int> j;
public:
    void push(int x) {
        i.push(x);
        auto t = j.find(x);
        if(t != j.end()){
            t->second++;
        }else{
            j[x] = 1;
        }
    }
    void pop() {
        int aux = i.top();
        i.pop();
        auto t = j.find(aux);
        if(t != j.end()){
            t->second--;
            if(t->second == 0){
                j.erase(t);
            }
        }
    }
    int top() {
        return i.top();
    }
    int getMin() {
        return j.begin()->first;
    }
};