#ifndef PROGRAM_CREEK_ALGORITHMS_2_REORDER_LIST_H
#define PROGRAM_CREEK_ALGORITHMS_2_REORDER_LIST_H

#include "../common/list.h"

namespace three {
  /**
   * Given a singly linked list L: L0→L1→ ... →Ln-1→Ln,
    reorder it to: L0→Ln→L1→Ln-1→L2→Ln-2→...
    For example, given {1,2,3,4}, reorder it to {1,4,2,3}. You must do this in-place without altering the nodes' values.
   * */
  class reorder_list {
  public:

    static void reorder(common::list_node<int> *a) {
      common::list_node<int>* slow = a;
      common::list_node<int>* fast = a;

      while(fast != nullptr && fast->next != nullptr && fast->next->next != nullptr) {
        slow = a->next;
        fast = a->next->next;
      }

      common::list_node<int>* second = slow->next;
      slow->next = nullptr;

      auto rev = common::list_node<int>::reverse(second);

      while(rev != nullptr) {
        common::list_node<int> * temp1 = a->next;
        common::list_node<int> * temp2 = rev->next;

        a->next = rev;
        rev->next = temp1;

        a = temp1;
        rev = temp2;
      }
    }

    static void test() {
      common::list_node<int> a1(1);
      common::list_node<int> a2(2);
      common::list_node<int> a3(3);
      common::list_node<int> a4(4);

      a1.next = &a2;
      a2.next = &a3;
      a3.next = &a4;

      reorder(&a1);

      a1.print();
    }
  };
}

#endif //PROGRAM_CREEK_ALGORITHMS_2_REORDER_LIST_H
