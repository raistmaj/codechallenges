struct Bucket{
  int low;
  int high;
  Bucket(){
      low = -1;
      high = -1;
  }
};
class Solution {
public:
    int maximumGap(vector<int>& nums) {
        if(nums.size() < 2){
            return 0;
        }
        int maximum = nums[0];
        int minimum = nums[0];
        for(int i = 1; i < nums.size(); ++i){
            maximum = std::max(maximum,nums[i]);
            minimum = std::min(minimum,nums[i]);
        }
        
        // Initializes an array of buckets
        std::vector<Bucket> buckets(nums.size() + 1); // for 0 to n
        
        double interval = (static_cast<double>(nums.size()) / static_cast<double>((maximum-minimum)));
        for(int i = 0; i < nums.size(); ++i){
            int index = static_cast<int>((nums[i]-minimum) * interval);
            
            if(buckets[index].low == -1){
                buckets[index].low = nums[i];
                buckets[index].high = nums[i];
            }else{
                buckets[index].low = std::min(buckets[index].low, nums[i]);
                buckets[index].high = std::max(buckets[index].high, nums[i]);
            }
        }
        // Scan buckets to find maximum gap
        int result = 0;
        int prev = buckets[0].high;
        for(int i = 1; i < buckets.size(); ++i){
            if(buckets[i].low != -1){
                result = std::max(result, buckets[i].low-prev);
                prev = buckets[i].high;
            }
        }
        return result;
    }
};