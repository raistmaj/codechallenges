bool isValidBST(TreeNode* root) {
    return isValidBST(root, std::numeric_limits<double>::min(), std::numeric_limits<double>::max());    
}
 
bool isValidBST(TreeNode* p, double min, double max){
    if(p==null) 
        return true;
 
    if(p->val <= min || p->val >= max)
        return false;
 
    return isValidBST(p->left, min, p->val) && isValidBST(p->right, p->val, max);
}