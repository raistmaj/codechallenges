class Solution {
public:
	bool se(int st, int ed, int target, std::vector<int>& A){
        if (st>ed) {return false;}
        else{
            int mid = st+(ed-st)/2;
            if (A[mid]==target){return true;}
             
            if (A[mid]>A[st]){
                if (target<=A[mid] && target>=A[st]){
                    return se(st,mid-1,target,A);
                }else{
                    return se(mid+1,ed,target,A);
                }
            }
             
            if (A[mid]<A[st]){
                 
                if (target<=A[mid] || target >= A[st]){
                    return se(st,mid-1,target,A);
                }else{
                    return se(mid+1,ed,target,A);
                }
                 
            }
             
            if (A[mid]==A[st]){return se(st+1,ed,target,A);}
             
            return false;
        }
         
    }
    bool search(vector<int>& nums, int target) {
        if (nums.size()==0){return false;}
        return  se(0,nums.size()-1,target,nums);
    }
};