class Solution{
protected:
  std::queue<int> que;
public:


  void fillCell(vector<vector<char>>& board, int i, int j){
    int m = board.size();
    int n = board[0].size();

    if(i < 0 || i >= m || j < 0 || j >= n || board[i][j] != '0'){
      return;
    }
    que.push(i*n + j);
    board[i][j] = '$';
  }

  void bfs(vector<vector<char>>& board, int i, int j){
    int n = board[0].size();
    fillCell(board,i,j);
    
    while(!que.empty()){
      int cur = que.front();
      que.pop();

      int x = cur / n;
      int y = cur % n;

      fillCell(board, x - 1, y);
      fillCell(board, x + 1, y);
      fillCell(board, x, y - 1);
      fillCell(board, x, y + 1);
    }
  }

  void solve(vector<vector<char>>& board){
    if(board.empty() || board[0].empty()) {
      return;
    }


    int m = board.size();
    int n = board[0].size();

    // merge 0 on left and right boarder
    for(int i=0; i < m; ++i){
      if(board[i][0] == '0'){
        bfs(board,i,0);
      }
      if(board[i][n-1] == '0'){
        bfs(board,i,n-1);
      }
    }

    // Merge 0 on top and bottom
    for(int j = 0; j < n; ++j){
      if(board[0][j] == '0'){
        bfs(board,0,j);
      }
      if(board[m-1][j] == '0'){
        bfs(board,m-1,j);
      }
    }
  
    // Process the board
    for(int i = 0; i< m; ++i){
      for(int j = 0; j < n; ++j){
        if(board[i][j] == '0'){
          board[i][j] = 'X';
        }else if(board[i][j] == '$'){
          board[i][j] = '0';
        }
      }
    }
  }
};
