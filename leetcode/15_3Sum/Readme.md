Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0?
Find all unique triplets in the array which gives the sum of zero.

Note:
Elements in a triplet (a,b,c) must be in non-descending order. (ie, a ≤ b ≤ c)
The solution set must not contain duplicate triplets.
    For example, given array S = {-1 0 1 2 -1 -4},

    A solution set is:
    (-1, 0, 1)
    (-1, -1, 2)

    ---------------

Clearly a dp programming algorithm, as we have to return the sets instead of index we can order the input as that will help to
increase the speed.

For the two sums, what we did was hash[target - value] == s[i] we insert the index. So now what we need to do is

For each number on S with index i
	set Negate as -1 * S[i]
	set start and end 
	until start > end
		if s[start] + s[end] == Negate --> we have a match
			push S[i], S[start], S[end]
		else if s[start] + s[end] < Negate // the input array was ordered and we need a bigger number
			++start
		else
			--end
	