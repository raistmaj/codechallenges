class Solution {
public:
  UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
    if (node == nullptr) {
      return nullptr;
    }
    queue<UndirectedGraphNode*> q;
    unordered_map<UndirectedGraphNode*, UndirectedGraphNode*> m;

    UndirectedGraphNode* newHead = new UndirectedGraphNode(node->label);
    q.push(node);
    m[node] = newHead;
    while (!q.empty()) {
      UndirectedGraphNode* current = q.front();
      q.pop();
      vector<UndirectedGraphNode *> &currNeighbors = current->neighbors;

      for (UndirectedGraphNode* aNeighbor : currNeighbors) {
        auto contains = m.find(aNeighbor);
        if (contains == m.end()) {
          UndirectedGraphNode* copy = new UndirectedGraphNode(aNeighbor->label);
          m[aNeighbor] = copy;
          m[current]->neighbors.push_back(copy);
          q.push(aNeighbor);
        }
        else {
          m[current]->neighbors.push_back(m[aNeighbor]);
        }
      }
    }
    return newHead;
  }
};