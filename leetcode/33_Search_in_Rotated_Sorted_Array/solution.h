#include <algorithm>

class Solution {
public:
  int search(vector<int> &nums, int target) {
    if (nums.empty())
      return -1;
    if (nums.size() == 1 && nums[0] == target)
      return 0;

    for (int i = 0; i < nums.size(); ++i) {
      if (nums[i] == target)
        return i; // if we find the value
    }
    return -1;
  }
};
