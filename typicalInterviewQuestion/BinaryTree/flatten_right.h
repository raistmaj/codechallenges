/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void flatten(TreeNode* root) {
        std::stack<TreeNode*> st;

        TreeNode* p = root;
        while(p != nullptr || !st.empty()){
        	if(p->right != nullptr){
        		st.push(p->right);
        	}
        	if(p->left != nullptr){
        		p->right = p->left;
        		p->left = nullptr;
        	}else if(!st.empty()){
        		TreeNode* tmp = st.top();
        		st.pop();
        		p->right = tmp;
        	}
        	p = p->right;
        }
    }
};