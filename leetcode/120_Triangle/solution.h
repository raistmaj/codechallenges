class Solution {
public:
    int minimumTotal(vector<vector<int>>& triangle) {
        std::vector<int> total(triangle.size());
        
    	int l = triangle.size() - 1;
    	for (int i = 0; i < triangle[l].size(); i++) {
    		total[i] = triangle[l][i];
    	}
    	// iterate from last second row
    	for (int i = triangle.size() - 2; i >= 0; i--) {
    		for (int j = 0; j < triangle[i + 1].size() - 1; j++) {
    			total[j] = triangle[i][j] + std::min(total[j], total[j + 1]);
    		}
    	}
     
    	return total[0];
    }
};