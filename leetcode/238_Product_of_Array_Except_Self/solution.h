class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        if(nums.empty()) return std::vector<int>();
        vector<int> output(nums.size());
        output[output.size()-1] = 1;
        for(int i = nums.size() - 2; i >= 0 ; --i) {
            output[i] = output[i+1] * nums[i+1];
        }
        int left = 1;
        for(int i = 0; i < nums.size(); ++i){
            output[i] *= left;
            left *= nums[i];
        }
        return output;
    }
};
