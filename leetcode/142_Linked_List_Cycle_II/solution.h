/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
       ListNode * first = head;
       ListNode * second = head;
        
       while(first != nullptr && second != nullptr)
       {
           first = first->next;
           second = second->next;
           if(second != nullptr)
               second = second->next;
           if(first == second)
               break;
         }
         
         if(second == nullptr) return nullptr;
         

         first = head;
         while(first!=second)
         {
             first = first->next;
             second = second->next;
         }
         
         return second;
    }
};