class Solution {
public:
  bool searchMatrix(vector<vector<int>> &matrix, int target) {
    for (int i = 0; i < matrix.size(); ++i) {
      if (matrix[i][0] > target)
        return false;
      bool ret = std::binary_search(matrix[i].begin(), matrix[i].end(), target);
      if (ret) {
        return true;
      }
    }
    return false;
  }
};
