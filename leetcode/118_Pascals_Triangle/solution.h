class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        std::vector<vector<int>> pascalT(numRows);
        for(int i = 0; i < numRows; ++i){
            pascalT[i] = std::vector<int>((i+1),0);
        }
        for(int i = 0; i < numRows; ++i){
            pascalT[i][0] = pascalT[i][pascalT[i].size()-1] = 1;
        }
        if(numRows > 2){
            for(int i = 2; i< numRows; ++i){
                for(int j = 1; j < pascalT[i].size() - 1; ++j){
                    pascalT[i][j] = pascalT[i-1][j-1] + pascalT[i-1][j];
                }
            }
        }
        return pascalT;
    }
};