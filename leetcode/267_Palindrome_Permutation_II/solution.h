class Solution {
public:
    //compute unique permutations of the first half palindrom
    void bt(string halfs, int n, int pos, vector<string> &ret, int odd_cnt, char odd_char){
        if(pos==n){
            string rev = halfs;
            reverse(rev.begin(), rev.end());
            if(odd_cnt) halfs += odd_char;
            ret.push_back(halfs+rev);
        }else{
            for(int i=pos; i<n; ++i){
                if(i>pos && halfs[i]==halfs[pos]) continue;
                swap(halfs[i], halfs[pos]);
                bt(halfs, n, pos+1, ret, odd_cnt, odd_char);
            }
        }
    }
 
    //compute first half of palindrome
    string getHalfS(unordered_map<char, int> &hash, char &odd_char){
        string halfs;
        for(auto h:hash){
            if(h.second%2==1){
                odd_char = h.first;
                h.second--;
            }
            for(int i=0; i<h.second/2; ++i){
                halfs += h.first;
            }
        }
        return halfs;
    }
 
    //compute the count of odd characters in string, 
    //if it is greater than 1, there's no palindrome permutaion
    int getOddCnt(string s, unordered_map<char, int> &hash){
        int odd_cnt = 0;
        for(auto c:s){
            hash[c]++;
            if(hash[c]%2==1){
                odd_cnt++;
            }else{
                odd_cnt--;
            }
        }
        return odd_cnt;
    }
 
 
    vector<string> generatePalindromes(string s) {
        vector<string> ret;
        unordered_map<char, int> hash;
        int odd_cnt = getOddCnt(s, hash);
        if(odd_cnt>1) return ret;
        char odd_char;
        string halfs = getHalfS(hash, odd_char);
        bt(halfs, halfs.size(), 0, ret, odd_cnt, odd_char);
        return ret;
    }
};