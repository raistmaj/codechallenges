class Solution {
public:
    int lengthOfLastWord(string s) {
        int longLW = 0;
        int actualSP = 0;
        int tamS = s.length();
        bool inWord = false;
        const char* sdata = s.data();
        while(*sdata){
            if(isalpha(*sdata)){
                if(inWord == false){
                    longLW = 0;
                }
                longLW++;
                inWord = true;
            }else if(isspace(*sdata)){
                inWord = false;
            }
            ++sdata;
        }
        return longLW;
    }
};