class Solution {
public:
    int compareVersion(string version1, string version2) {
        std::vector<int> arr1, arr2;
        std::stringstream ss(version1);
        std::string auxString;
        while(std::getline(ss,auxString,'.')){
          arr1.push_back(std::stoi(auxString));
        }
        ss.clear();
        std::stringstream ss2(version2);
        auxString.clear();
        while(std::getline(ss2,auxString,'.')){
          arr2.push_back(std::stoi(auxString));
        }
    
        // Ok now we have splitted each string
        size_t i = 0;
        while( i < arr1.size() || i < arr2.size()){
          if(i < arr1.size() && i < arr2.size()){
            int num1 = arr1[i];
            int num2 = arr2[i];
            if( num1 > num2){
              return 1;
            }else if(num1 < num2){
              return -1;
            }
          }else if(i < arr1.size()){
            if(arr1[i] != 0){ 
              return 1;
            }
          }else if(i < arr2.size()){
            if(arr2[i] != 0){
              return -1;
            }
          }
          ++i;
        }
        return 0;
    }
};