#ifndef _INTERVAL_MAP_HPP_
#define _INTERVAL_MAP_HPP_

#include <limits>
#include <map>
/**
  \Author José Gerardo Palma Durán <jpalma@barracuda.com>
  An interval-map is a mutable data structure that maps half-open intervals
  of exact keys to values. An interval-map is queried at a discrete point,
  and the result of the query is the value mapped to the interval containing the point
  There are some pros and cons using this class.
  Pros:
    - You don't need to know or store all the values you need, this in some cases is nearly
      impossible, for example random funcition of real numbers that map its result into
      function pointers or factories.
    - The speed in those cases is blazing fast, log(n)
    - You use less resources.
  Cons:
    - If you know the whole set of values a set or map will be faster. This is log(n) + k1
      and the others are log(n) + k2, been k2 < than k1.
  \pre The numeric limits of key must exits
  std::numeric_limits<Tkey>.min
  std::numeric_limits<Tkey>.max
  The get interval method is log(n) + c = log(n)
 */
template<class Tkey, class Tvalue>
class intervalmap : public std::map<Tkey,Tvalue>
{
    typedef typename std::map<Tkey,Tvalue>::const_iterator _cit;
public:
    /**
     Creates a new instance of the object
     */
    intervalmap() : std::map<Tkey,Tvalue>() {
    }
    /**
     Copy constructor
     Creates a copy of the desired interval map into our new object
     */
    intervalmap ( const intervalmap<Tkey,Tvalue> & val ) : std::map<Tkey,Tvalue> ( val ) {
    }
    /**
     Clear the resources gathered by the instance of the class
     */
    ~intervalmap() {
    }

    /**
     Add a new key to the interval.
     If the interval already exits is overwritten.
    \param val New pair with the desired interval
    */
    void addvalue ( const std::pair<Tkey,Tvalue> & val ) {
        this->operator[] ( val.first ) = val.second;
    }
    /**
     Get the range keys of an key, that means the keys that bounds the val key.
     \param val With the key we are looking for
     \return If the pair is lower unbound (-min_value,firstkey]
             If the pair fits exactily with the val [val,val]
             If the pair if in an interval [lower_bound,upper_bound), Our val is between them, that means,  lower_bound <= val <= upper_bound
             If the pair is upper unbound [lastkey,max_value)
    */
    std::pair<Tkey,Tkey> getinterval ( const Tkey & val ) const {
        _cit it = this->lower_bound ( val );
        if ( it == this->end() ) {
            _cit it2 = it;
            it2--;
            return std::pair<Tkey,Tkey> ( ( *it2 ).first,std::numeric_limits<Tkey>::max() );
        } else if ( val == ( *it ).first ) {
            return std::pair<Tkey,Tkey> ( ( *it ).first, ( *it ).first );
        } else if ( it == this->begin() ) {
            return std::pair<Tkey,Tkey> ( std::numeric_limits<Tkey>::min(), ( *it ).first );
        } else {
            _cit it2 = it;
            --it2;
            return std::pair<Tkey,Tkey> ( ( *it2 ).first, ( *it ).first );
        }
    }
    /**
     Get the range keys of an key, that means the keys that bounds the val key. With this method we return only
     positions, that means, const_iterator of the map.
     If one points to begin that means numeric_limit<Tvalue>::min()
     If one points to end that means numeric_limits<Tvalue>::max()
     \param val With the key we are looking for
     \return If the pair is lower unbound (begin,firstkey]
             If the pair fits exactily with the val [val,val]
             If the pair if in an interval [lower_bound,upper_bound), Our val is between them, that means,  lower_bound <= val <= upper_bound
             If the pair is upper unbound [lastkey,end)
    */
    const std::pair<_cit,_cit> getintervalit ( const Tkey & val ) const {
        _cit it = this->lower_bound ( val );
        if ( it == this->end() ) {
            _cit it2 = it;
            it2--;
            return std::pair<_cit,_cit> ( ( *it2 ),this->end() );
        } else if ( val == ( *it ).first ) {
            return std::pair<_cit,_cit> ( ( *it ), ( *it ) );
        } else if ( it == this->begin() ) {
            return std::pair<_cit,_cit> ( this->begin(), ( *it ) );
        } else {
            _cit it2 = it;
            --it2;
            return std::pair<_cit,_cit> ( ( *it2 ), ( *it ) );
        }
    }
    /**
      Get the value requested in one area of the interval
      \param val With the value into one interval que are looking for.
      \return The value of that key in the interval
    */
    const Tvalue get ( const Tkey & val ) const {
        _cit it = this->lower_bound ( val );
        if ( it == this->end() ) {
            _cit it2 = it;
            it2--;
            return ( *it2 ).second;
        } else if ( val == ( *it ).first ) {
            return ( *it ).second;
        } else if ( it == this->begin() ) {
            return ( *it ).second;;
        } else {
            _cit it2 = it;
            --it2;
            return ( *it2 ).second;
        }
    }
protected:
};
/**
 Example:

	Suposse the following class hierarchy

	Object
	|-	ObjectA
	|-	ObjectB

	With their corresponding factories

	ObjectFactory
	|-	ObjectFactoryA
	|-	ObjectFactoryB

	We will create an interval map to map the creation of objects into some 32 bit precision real data type.
	For that we use float datatype and factories

	class Object
	{
	public:
		virtual void Sing() const
		{
		}
	};
	class ObjectA : public Object
	{
	public:
		void Sing() const
		{
			std::cout << "ROARRRR" << std::endl;
		}
		int roar;
	};
	class ObjectB : public Object
	{
	public:
		void Sing() const
		{
			std::cout << "WOOF WOOF" << std::endl;
		}
		int woofwoof;
	};

	class ObjectFactory
	{
	public:
		virtual Object* CreateObject()
		{
			return 0;
		}
	};

	class ObjectFactoryA : public ObjectFactory
	{
	public:
		Object* CreateObject()
		{
			return new ObjectA();
		}
	};

	class ObjectFactoryB : public ObjectFactory
	{
	public:
		Object* CreateObject()
		{
			return new ObjectB();
		}
	};

	// This is the code we want to test
	// The code use shared_ptr from boost library, feel free to remove them or use the c++11 shared_ptr in the memory library
	// It is only an example with heavy template programming and oo design.

	intervalmap<float,boost::shared_ptr<ObjectFactory> > example;
	example.addvalue(std::make_pair<float,boost::shared_ptr<ObjectFactory> >(1,boost::shared_ptr<ObjectFactoryA>(new ObjectFactoryA() ) ) );
	example.addvalue(std::make_pair<float,boost::shared_ptr<ObjectFactory> >(2,boost::shared_ptr<ObjectFactoryB>(new ObjectFactoryB() ) ) );

	boost::shared_ptr<Object> objectA = boost::shared_ptr<Object>( example.get(1)->CreateObject() );
	boost::shared_ptr<Object> objectB = boost::shared_ptr<Object>( example.get(2.5)->CreateObject() );

	objectA->Sing();
	objectB->Sing();

	std::cout << typeid( *(objectA.get()) ).name() << std::endl;
	std::cout << typeid( *(objectB.get()) ).name() << std::endl;

	// The console ouput is:

	ROARRRR
	WOOF WOOF
	7ObjectA
	7ObjectB
 */
#endif

