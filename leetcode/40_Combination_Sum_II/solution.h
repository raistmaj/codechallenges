class Solution {
public:
    void getCombination(std::vector<int>& candidates, int start, int target, std::vector<int> temp, std::vector<std::vector<int>>& result){
        if(target == 0){
            result.push_back(temp);
        }else{
            for(int i = start; i < candidates.size(); ++i){
                if(target < candidates[i]){
                    continue;
                }
                
                temp.push_back(candidates[i]);
                getCombination(candidates, i + 1 , target - candidates[i] , temp, result);
                temp.pop_back();
            }
        }
    }

    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
        std::vector<std::vector<int>> result;
        if(candidates.empty()){
            return result;
        }
        std::sort(candidates.begin(),candidates.end());
        std::vector<int> temp;
        getCombination(candidates, 0, target, temp, result);
        
        std::set<std::vector<int>> out(result.begin(),result.end());
        
        result.clear();
        result.insert(result.begin(), out.begin(), out.end());
        return result;
    }
};