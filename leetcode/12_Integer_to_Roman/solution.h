class Solution {
public:
    // Romans didn't know 0
    char* units[10] = {"","I","II","III","IV","V","VI","VII","VIII","IX"};
    char* decs[10] =  {"","X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    char* cent[10] =  {"","C", "CC" , "CCC", "CD", "D" ,"DC", "DCC", "DCCC", "CM"};
    char* mill[4] =  {"", "M", "MM" , "MMM"};
    
    string intToRoman(int num) {
        int dec;
        int i = 0;
        std::string retVal;
        if(num!=0){
            // units
            dec = num % 10;
            retVal += units[dec];
            num *= 0.1;
            if(num != 0){
                // Decs
                dec = num %10;
                retVal = decs[dec] + retVal;
                num*=0.1;
                if(num != 0){
                    // Cents
                    dec = num % 10;
                    retVal = cent[dec] + retVal;
                    num *= 0.1;
                    if(num != 0){
                        dec = num % 10;
                        retVal = mill[dec] + retVal;
                    }
                }
            }
        }
        return retVal;
    }
};