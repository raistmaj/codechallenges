#include <vector>

class Solution {
public:
    int missingNumber(vector<int>& nums) {
        if(nums.empty()) return 0;
        // It is faster with std::sort but the worst case could be O(n²)
        std::make_heap(nums.begin(),nums.end()); // 3 O(n) -> O(n)
        std::sort_heap(nums.begin(),nums.end()); // O(n)
        for(int i = 0; i < nums.size(); ++i){
            if(nums[i] != i){
                return i;
            }
        }
        return nums.size();
    }
};
