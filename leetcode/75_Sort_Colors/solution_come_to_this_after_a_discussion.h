class Solution {
public:
    void sortColors(vector<int>& nums) {
        int red=0;
        int blue=nums.size()-1;
         
        while (nums[red]==0){red++;}
        while (nums[blue]==2){blue--;}
         
        int i=red;
        while (i<=blue){
            if (nums[i]==0 && i>red) {
            	std::swap(nums[i],nums[red]);
            	red++;
            	continue;
            }
            if (nums[i]==2) {
            	std::swap(nums[i],nums[blue]);
            	blue--;
            	continue;
            }
            i++;
        }
    }
};