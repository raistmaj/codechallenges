Given a linked list, remove the nth node from the end of list and return its head.

For example,

   Given linked list: 1->2->3->4->5, and n = 2.

   After removing the second node from the end, the linked list becomes 1->2->3->5.
Note:
Given n will always be valid.
Try to do this in one pass.

-----------

Two pointers fast and slow

1-2-3-4-5
F
S

move Fast N positions

1-2-3-4-5
    F
S

Move both until we reach the end with fast-next!


1º

1-2-3-4-5
      F
  S

2º

1-2-3-4-5
        F
    S

Adjust slow->next to slow->next->next (we should remove 4)

1-2-3-5




