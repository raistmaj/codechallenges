#ifndef PROGRAM_CREEK_ALGORITHMS_3_ISOMORPHIC_STRINGS_H
#define PROGRAM_CREEK_ALGORITHMS_3_ISOMORPHIC_STRINGS_H

#include <string>
#include <unordered_map>
#include <vector>

namespace one {

  /**
   *  Given two strings s and t, determine if they are isomorphic.
   *  Two strings are isomorphic if the characters in s can be replaced to get t.
   *  For example,"egg" and "add" are isomorphic, "foo" and "bar" are not
   * */
  class isomorphic_strings {
  public:
    static bool isomorphic(const std::string &s, const std::string &t) {
      if (s.size() != t.size()) {
        return false;
      }
      std::vector<int> first(s.size(), -1);
      std::vector<int> second(t.size(), -1);

      {
        int actual_index = 0;
        std::unordered_map<char, int> map_values;
        for (std::size_t i = 0; i < s.size(); ++i) {
          auto found = map_values.find(s[i]);
          if (found != map_values.end()) {
            first[i] = found->second;
          } else {
            first[i] = actual_index;
            map_values.insert({s[i], actual_index});
            actual_index++;
          }
        }
      }
      {
        int actual_index = 0;
        std::unordered_map<char, int> map_values;
        for (std::size_t i = 0; i < t.size(); ++i) {
          auto found = map_values.find(t[i]);
          if (found != map_values.end()) {
            second[i] = found->second;
          } else {
            second[i] = actual_index;
            map_values.insert({t[i], actual_index});
            actual_index++;
          }
        }
      }
      return first == second;
    }

    static void test() {
      std::cout << isomorphic("egg", "add") << " - 1\n"
                << isomorphic("foo", "bar") << " - 0\n";
    }
  };
}


#endif //PROGRAM_CREEK_ALGORITHMS_3_ISOMORPHIC_STRINGS_H
