
class Solution {
public:
    string multiply(string num1, string num2) {
        string res(num1.size()+num2.size(),'0');
        for(int i=num1.size()-1;i>=0;i--){
            int c=0;
            for(int j=num2.size()-1;j>=0;j--){
                int t=(num1[i]-'0') * (num2[j]-'0') + c + res[res.size()-1-(num1.size()-1-i)-(num2.size()-1-j)]-'0';
                res[res.size()-1-(num1.size()-1-i)-(num2.size()-1-j)]=t%10+'0';
                c=t/10;
            }
            if(c!=0)
                res[res.size()-1-(num1.size()-1-i)-(num2.size()-1)-1]=c+'0';
        }
        while(res.size()>1 && res[0]=='0')
            res=res.substr(1,res.size()-1);
        return res;
    }
};