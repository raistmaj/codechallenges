Given a m x n matrix, if an element is 0, set its entire row and column to 0. Do it in
place.

This problem can solve by following 4 steps:
• check if first row and column are zero or not
• mark zeros on first row and column
• use mark to set elements
• set first column and row
