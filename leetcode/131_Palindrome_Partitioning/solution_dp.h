class Solution {
public:
	vector<vector<string>> partition(string s) {
        vector<vector<string>> result;
        if(s.length() == 0){
        	return result;
        }
        if(s.length() <= 1){
        	result.push_back(std::vector<string>(1,s));
        	return result;
        }
        int length = s.length();
        std::vector<std::vector<int>> table(length,std::vector<int>(table));
        for(int l = 1; l <= length; ++l){
        	for(int i = 0; i <= length - l; ++i){
        		int j = i + l - 1;
        		if(s[i] == s[j]){
        			if(l == 1 || l == 2){
        				table[i][j] = 1;
        			}else{
        				table[i][j] = table[i+1][j-1];
        			}
        			if(table[i][j] == 1){
        				result.push_back(std::vector<string>(s.substr(i,j+1)));
        			}
        		}else{
        			table[i][j] = 0;
        		}
        	}
        }
        return result;
    }
};