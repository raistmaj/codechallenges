#include <deque>

class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        if(digits.empty()){ return std::vector<int>({1}); }
        std::deque<int> retVal;
        int carry = 0;
        int suma = 1;
        for(int i = digits.size() - 1; i >= 0 ; --i){
            int actualNum = digits[i] + suma + carry;
            suma = 0;
            if(actualNum >= 10){
                actualNum -= 10;
                carry = 1;
            }else{
                carry = 0;
            }
            retVal.push_front(actualNum);
            if( carry == 0 ) { 
                retVal.insert(retVal.begin(),digits.begin(),digits.begin()+i);
                return  std::vector<int>(retVal.begin(),retVal.end()); 
            }
        }
        if(carry == 1){
            retVal.push_front(1);
        }
        return std::vector<int>(retVal.begin(),retVal.end());
    }
};