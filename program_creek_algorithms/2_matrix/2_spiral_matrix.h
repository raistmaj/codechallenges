#ifndef PROGRAM_CREEK_ALGORITHMS_2_SPIRAL_MATRIX_H
#define PROGRAM_CREEK_ALGORITHMS_2_SPIRAL_MATRIX_H

#include <iostream>
#include <vector>

namespace two {
  /**
   *  Given a matrix of m x n elements (m rows, n columns), return all elements of the matrix in spiral order.

      For example, given the following matrix:
      [
       [ 1, 2, 3 ],
       [ 4, 5, 6 ],
       [ 7, 8, 9 ]
      ]
      You should return [1,2,3,6,9,8,7,4,5].
   * */
  class spiral_matrix {
  public:
    static std::vector<int> spiral(const std::vector<std::vector<int>> &input) {
      if (input.empty()) { return std::vector<int>(); }
      if (input[0].empty()) { return std::vector<int>(); }

      std::size_t actual_position = 0;
      std::size_t size_of_result = input.size() * input[0].size();
      std::vector<int> ret_val;

      int left = 0;
      int right = input[0].size() - 1;
      int top = 0;
      int bottom = input.size() - 1;
      while (ret_val.size() < size_of_result) {
        // Move to the right
        for (int i = left; i <= right; ++i) {
          ret_val.push_back(input[top][i]);
        }
        top++;
        // Move bottom
        for (int i = top; i <= bottom; ++i) {
          ret_val.push_back(input[i][right]);
        }
        right--;

        if (bottom < top)
          break;

        // move to the left
        for (int i = right; i >= left; --i) {
          ret_val.push_back(input[bottom][i]);
        }
        bottom--;

        if (right < left)
          break;

        // move top
        for (int i = bottom; i >= top; --i) {
          ret_val.push_back(input[i][left]);
        }
        left++;
      }
      return ret_val;
    }

    static void test() {
      std::vector<std::vector<int>> input{
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
      };

      auto output = spiral(input);
      for (auto &i: output) {
        std::cout << i << " ";
      }
      std::cout << '\n';
    }
  };

  class spiral_creator {
  public:
    static std::vector<std::vector<int>> generate(int N) {
      std::vector<std::vector<int>> ret_val(N, std::vector<int>(N, 0));

      int actual_value = 1;
      int total_number = N * N;
      int left = 0;
      int right = N - 1;
      int top = 0;
      int bottom = N - 1;

      while(actual_value <= total_number) {
        for (int i = left; i <= right; ++i) {
          ret_val[top][i] = actual_value++;
        }
        top++;

        for (int i = top; i <= bottom; ++i) {
          ret_val[i][right] = actual_value++;
        }
        right--;

        for (int i = right; i >= left; --i) {
          ret_val[bottom][i] = actual_value++;
        }
        bottom--;

        for (int i = bottom; i >= top; --i) {
          ret_val[i][left] = actual_value++;
        }
        left++;
      }
      return ret_val;
    }

    static void test() {
      auto output(generate(4));

      for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
          std::cout << output[i][j] << " ";
        }
        std::cout << '\n';
      }
      std::cout << "\n";
    }
  };
}

#endif //PROGRAM_CREEK_ALGORITHMS_2_SPIRAL_MATRIX_H
