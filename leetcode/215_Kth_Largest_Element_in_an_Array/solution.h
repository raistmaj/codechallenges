class Solution{
public:
  int findKthLargest(std::vector<int> nums, int k){
    if(nums.empty()) return 0;
    if(k > nums.size()) return 0;
    // AVG time O(nlogn)
    std::sort(nums.begin(),nums.end());
    return nums[nums.size() - k];
  }

  int getKth(int k, std::vector<int>& nums, int start, int end){
    int pivot = nums[end];

    int left = start;
    int right = end;

    while(1){
      while(nums[left] < pivot && left < right){
        left++;
      }
      while(nums[right] >= pivot && right > left){
        right--;
      }
      if(left == right){
        break;
      }
      std::swap(nums[left],nums[right]);
    }

    if(k == left + 1){
      return pivot;
    }else if(k < left + 1){
      getKth(k,nums,start,left - 1);
    }else{
      getKth(k,nums,left+1,end);
    }
  }

  int findKthLargest(std::vector<int> nums, int k){
    if(nums.empty()) return 0;
    if(k < 1) return 0;

    return getKth(nums.size() - k + 1, nums, 0, nums.size() - 1);
  }
};
