#include <bitset>

class Solution {
public:
  uint32_t reverseBits(uint32_t n) {
    std::bitset<32> input(n);
    std::bitset<32> output(0);
    for (int i = 0, j = 31; i < 32 && j >= 0; ++i, --j) {
      output.set(j, input[i]);
    }
    return static_cast<uint32_t>(output.to_ulong());
  }
};
