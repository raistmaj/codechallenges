class Solution {
public:
    int countPrimes(int n) {
       if(n <= 2) return 0;
       std::vector<int> primes(n,1);
       for(int i = 0; i < 2; ++i){
           primes[i] = 0;
       }
       for(int i = 2; i * i < n; ++i){
           if(primes[i]==0) continue;
           for(int j = i * i; j < n; j+=i){
               primes[j] = 0;
           }
       }
       int count = 0;
       for(int i = 2; i < n; ++i){
           if(primes[i]) count++;
       }
       return count;
    }
};