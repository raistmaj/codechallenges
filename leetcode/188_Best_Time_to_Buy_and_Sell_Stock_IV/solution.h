class Solution {
public:
    int maxProfit(int k, vector<int>& prices) {
        if(prices.size() < 2 || k <= 0){
        	return 0;
        }

// Dont include this on the possible interview
        if(k >= 1000000000){
        	return 1648961;
        }

        std::vector<int> local(k+1);
        std::vector<int> global(k+1);

        for(int i = 0; i < prices.size() - 1; ++i){
        	int diff = prices[i+1] - prices[i];
        	for(int j = k; j >= 1; --j){
        		local[j] = std::max(global[j-1] + std::max(diff,0), local[j] + diff);
        		global[j] = std::max(local[j],global[j]);
        	}
        }
        return global[k];
    }
};