class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) {
        bool firstRowZero = false;
        bool firstColumnZero = false;
        int row = matrix.size();
        if(row == 0){return;}
        int column = matrix[0].size();
        if(column == 0){ return;}
        // Mark the possible row/colum as 0
        for(size_t i = 0; i < row; ++i){
          if(matrix[i][0] == 0){
            firstColumnZero = true;
            break;
          }
        }
        for(size_t i = 0; i < column; ++i){
          if(matrix[0][i] == 0){
            firstRowZero = true;
            break;
          }
        }
    
        // Mark zeros on first row and column
        for(size_t i = 1; i < row; ++i){
          for(size_t j = 1; j < column; ++j){
            if(matrix[i][j] == 0){
              matrix[i][0] = 0;
              matrix[0][j] = 0;
            }
          }
        }
        // Set the diagonal
        for(size_t i = 1; i < column; ++i){
            if(matrix[0][i] == 0){
                for(int j = 0; j < row; ++j){
                    matrix[j][i] = 0;
                }
            }
        }
        for(size_t i = 1; i < row; ++i){
            if(matrix[i][0] == 0){
                for(int j = 0; j < column; ++j){
                    matrix[i][j] = 0;
                }
            }
        }
        // set first column and row
        if(firstColumnZero){
          for(size_t i = 0; i < row; ++i){
            matrix[i][0] = 0;
          }
        }
    
        if(firstRowZero){
          for(size_t i = 0; i < column; ++i){
            matrix[0][i] = 0;
          }
        }
    }
};