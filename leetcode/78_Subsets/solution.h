class Solution {
public:
 
    string getbinary(int d,int len){
        string str="";
        while (d>0){
            str=str+char((d%2)+'0');
            d=d/2;
        }   
        for(int i=0;i<len-str.size();i++){
         str=str+'0';   
        }
        return str;
    }
    vector<vector<int> > subsets(vector<int> &S) {
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        sort(S.begin(),S.end());
        vector<vector<int> > res;
        int n = S.size();
        for (int i=0;i<pow(2,n);i++){
            string str = getbinary(i,n);
            vector<int> ss;
            for (int j=0;j<n;j++){
                if (str[j]=='1'){
                    ss.push_back(S[j]);
                }
            }
            res.push_back(ss);
        }
        return res;
         
    }
};