/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        if ((head==nullptr) || (head->next == nullptr) ){return head;}
        ListNode pre(0);
        pre.next = head;
        head = &pre;
        ListNode* p = head;
                     
        while(p->next!=nullptr){
            ListNode *p2 = p->next;
            while ((p2->next!=nullptr)&&(p2->val==p2->next->val)){
                p2=p2->next;
            }
            if (p2!=p->next){
                p->next=p2->next;
            }else{
                p=p->next;
            }
        }  
        return head->next;
    }
};