/**
 * Definition for singly-linked list with a random pointer.
 * struct RandomListNode {
 *     int label;
 *     RandomListNode *next, *random;
 *     RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
 * };
 */
class Solution {
public:
    RandomListNode *copyRandomList(RandomListNode *head) {
        if(head == nullptr){
            return nullptr;
        }
        std::unordered_map<RandomListNode*,RandomListNode*> hash;
        RandomListNode* newHead = new RandomListNode(head->label);

        RandomListNode* p = head;
        RandomListNode* q = newHead;

        hash[head] = newHead;
        p = p->next;
        while(p != nullptr){
        	RandomListNode* tmp = new RandomListNode(p->label);
        	hash[p] = tmp;
        	q->next = tmp;
        	q = tmp;
        	p = p->next;
        }

        p = head;
        q = newHead;
        while(p != nullptr){
        	if(p->random != nullptr){
        		q->random = hash[p->random];
        	}else{
        		q->random = nullptr;
        	}
        	p = p->next;
        	q = q->next;
        }
        return newHead;
    }
};