#include <unordered_set>

class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        std::unordered_set<int> pd;
        for(int i = 0; i < nums.size(); ++i){
            auto it = pd.find(nums[i]);
            if(it == pd.end()){
                pd.insert(nums[i]);
            }else{
                return true;
            }
        }
        return false;
    }
};